<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<title>Invoice</title>
	<style>
		@media print {
		.noprint { display: none; }
		body,html{
			margin: 0;
			padding: 0;
			width: 100%;
			height: 100%;
		}
		}
		@page{
			margin: 0;
		}
	</style>
</head>

<body>
    @php
        $adminSetting = adminSetting();
    @endphp
	<table width="100%" cellpadding="0" cellspacing="0" align="center" style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; white-space: nowrap;">
		<tr>
			<td style="vertical-align: top;" align="center">
				<table cellpadding="15" cellspacing="0" align="center" style="border: 1px solid #d8d7d7; width: 740px; margin: 20px auto;">
					<tr>
						<td style="text-align: center;">
							<h1 style="font-size: 20px;line-height: 19px; color: #181712; margin-bottom: 0;text-decoration: underline;">Invoice Details</h1>
						</td>
					</tr>
					<tr>
						<td>
							<table width="100%" height="100%" cellpadding="10" cellspacing="0"
								style="border-bottom: 1px solid #ddd;" align="center">
								<tr>
									<td>
										<table width="100%" cellpadding="0" cellspacing="0" align="center">
											<tr align="middle">
												<td align="left">
													<a href="javacript:void(0);" target="_blank"
														style="text-decoration: none;">
														@isset($adminSetting['image'])
                                                            <img src="{{ asset('storage/'.$adminSetting['image']) }}" height="20" alt="logo" style="width: 100px; height: auto;display: block;">
                                                        @endisset
													</a>
												</td>
												<td align="right">
													<table align="right" cellpadding="0" cellspacing="0">
														<tr style="text-align: left;">
															<td>
																<p style="font-size: 13px;line-height: 22px; margin: 0; letter-spacing: 0.8px;">
																	<strong style="margin-right: 10px;">Invoice No :</strong></p>
															</td>
															<td>
																<p style="font-size: 13px;line-height: 22px; margin: 0; letter-spacing: 0.8px;">
																	#0000{{ $order->id }}</p>
															</td>
														</tr>
														<tr style="text-align: left;">
															<td>
																<p style="font-size: 13px;line-height: 22px; margin: 0; letter-spacing: 0.8px;">
																	<strong style="margin-right: 10px;">Invoice date:</strong></p>
															</td>
															<td>
																<p style="font-size: 13px;line-height: 22px; margin: 0; letter-spacing: 0.8px;">
																	{{ $order->created_at->format('F j, Y H:i:m') }}</p>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>
							<table width="100%" height="100%" cellpadding="0" cellspacing="0">
								<tr style="vertical-align: top;">
									<td style="width: 50%;">
										<table cellpadding="0" cellspacing="0">
											<tr>
												<td colspan="2">
													<h4 style="margin-top: 0px; margin-bottom: 15px; text-transform: uppercase; font-size: 15px;">Customer Infortamtion</h4>
												</td>
											</tr>
											<tr style="text-align: left;">
												<td>
													<p style="font-size: 13px;line-height: 22px; margin: 0; letter-spacing: 0.8px;">
														<strong style="margin-right: 10px;">Name :</strong>
													</p>
												</td>
												<td>
													<p style="font-size: 13px;line-height: 22px; margin: 0; letter-spacing: 0.8px;">{{ $user->name }}</p>
												</td>
											</tr>
											<tr style="text-align: left;">
												<td>
													<p style="font-size: 13px;line-height: 22px; margin: 0; letter-spacing: 0.8px;">
														<strong style="margin-right: 10px;">Email</strong>
													</p>
												</td>
												<td>
													<p style="font-size: 13px;line-height: 22px; margin: 0; letter-spacing: 0.8px;">{{ $user->email }}</p>
												</td>
											</tr>
											<tr style="text-align: left;">
												<td>
													<p style="font-size: 13px;line-height: 22px; margin: 0; letter-spacing: 0.8px;">
														<strong style="margin-right: 10px;">Phone:</strong>
													</p>
												</td>
												<td>
													<p style="font-size: 13px;line-height: 22px; margin: 0; letter-spacing: 0.8px;">
														{{ $user->phone }}</p>
												</td>
											</tr>
											<tr style="text-align: left; vertical-align: top;">
												<td>
													<p style="font-size: 13px;line-height: 22px; margin: 0; letter-spacing: 0.8px;">
														<strong style="margin-right: 10px;">Address:</strong></p>
												</td>
												<td>
													<p style="font-size: 13px;line-height: 22px; margin: 0; letter-spacing: 0.8px; white-space: initial !important;">
														567, South Applegate Lane Farmingdale, NY 11735</p>
												</td>
											</tr>
										</table>

									</td>
									<td style="width: 50%;">
										<table align="right" cellpadding="0" cellspacing="0">
											<tr>
												<td colspan="2">
													<h4 style="margin-top: 0px; margin-bottom: 15px; text-transform: uppercase; font-size: 15px;">Restaurant Infortamtion</h4>
												</td>
											</tr>
											<tr style="text-align: left;">
												<td>
													<p style="font-size: 13px;line-height: 22px; margin: 0; letter-spacing: 0.8px;">
														<strong style="margin-right: 10px;">Name:</strong></p>
												</td>
												<td>
													<p style="font-size: 13px;line-height: 22px; margin: 0; letter-spacing: 0.8px;">
														{{ optional($order->restaurant)->name }}</p>
												</td>
											</tr>
											<tr style="text-align: left;">
												<td>
													<p
														style="font-size: 13px;line-height: 22px; margin: 0; letter-spacing: 0.8px;">
														<strong style="margin-right: 10px;">Email:</strong></p>
												</td>
												<td>
													<p style="font-size: 13px;line-height: 22px; margin: 0; letter-spacing: 0.8px;">
														{{ optional($order->restaurant)->email }}</p>
												</td>
											</tr>
											<tr style="text-align: left;">
												<td>
													<p style="font-size: 13px;line-height: 22px; margin: 0; letter-spacing: 0.8px;">
														<strong style="margin-right: 10px;">Phone:</strong></p>
												</td>
												<td>
													<p style="font-size: 13px;line-height: 22px; margin: 0; letter-spacing: 0.8px;">
														{{ optional($order->restaurant)->mobile }}</p>
												</td>
											</tr>
                                            <tr style="text-align: left; vertical-align: top;">
												<td>
													<p style="font-size: 13px;line-height: 22px; margin: 0; letter-spacing: 0.8px;">
														<strong style="margin-right: 10px;">Table:</strong></p>
												</td>
												<td>
													<p style="font-size: 13px;line-height: 22px; margin: 0; letter-spacing: 0.8px; white-space: initial !important;">
														{{ optional($order->table)->name }}</p>
												</td>
											</tr>
											<tr style="text-align: left; vertical-align: top;">
												<td>
													<p style="font-size: 13px;line-height: 22px; margin: 0; letter-spacing: 0.8px;">
														<strong style="margin-right: 10px;">Address:</strong></p>
												</td>
												<td>
													<p style="font-size: 13px;line-height: 22px; margin: 0; letter-spacing: 0.8px; white-space: initial !important;">
														{{ optional($order->restaurant)->address }}</p>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>
							<table width="100%" height="100%" cellpadding="0" cellspacing="0">
								<tr style="vertical-align: middle; color: #ffffff; background: #2b3e50;">
									<th align="left"
										style="border-bottom: 1px solid #ddd; padding: 8px; width: 40%; ">
										<p style="font-size: 14px;margin: 0; letter-spacing: 0.8px;">
											Item</p>
									</th>
									<th align="left"
										style="border-bottom: 1px solid #ddd; padding: 8px;">
										<p style="font-size: 14px;margin: 0; letter-spacing: 0.8px;">
											Item Price</p>
									</th>
									<th align="left"
										style="border-bottom: 1px solid #ddd; padding: 8px;">
										<p style="font-size: 14px;margin: 0; letter-spacing: 0.8px;">
											Addon Price</p>
									</th>
									<th align="left"
										style="border-bottom: 1px solid #ddd; padding: 8px;">
										<p style="font-size: 14px;margin: 0; letter-spacing: 0.8px;">
											Quantity</p>
									</th>
									<th align="right"
										style="border-bottom: 1px solid #ddd; padding: 8px;">
										<p style="font-size: 14px;margin: 0; letter-spacing: 0.8px;">
											Total</p>
									</th>
								</tr>
                                @foreach ($order->order_details as $detail)
								<tr style="vertical-align: top;">
									<td style="border-bottom: 1px solid #eee; padding: 15px 7px;">
										<p style="font-size: 13px;margin: 0; letter-spacing: 0.8px; white-space: initial !important;">
											{{ optional( $detail->item)->name }}</p>
									</td>
									<td style="border-bottom: 1px solid #eee; padding: 15px 7px;">
										<p style="font-size: 13px;margin: 0; letter-spacing: 0.8px;">
											¢ {{ $detail->item_price }}</p>
									</td>
									<td style="border-bottom: 1px solid #eee; padding: 15px 7px;">
										<p style="font-size: 13px;margin: 0; letter-spacing: 0.8px;">
											¢ {{ $detail->addon_price }}</p>
									</td>
									<td style="border-bottom: 1px solid #eee; padding: 15px 7px;">
										<p style="font-size: 13px;margin: 0; letter-spacing: 0.8px;">
											{{ $detail->quantity }}</p>
									</td>
									<td align="right" style="border-bottom: 1px solid #eee; padding: 15px 7px;">
										<p style="font-size: 13px;margin: 0; letter-spacing: 0.8px;">
                                            ¢ {{ $detail->total_price }}</p>
									</td>
								</tr>
                                @endforeach

								<tr style="vertical-align: bottom;">
									<td colspan="1"></td>
									<td colspan="2" align="right"  style=" padding: 15px 7px; border-bottom: 1px solid #eee; "><p style="font-size: 13px; margin: 0; letter-spacing: 0.8px;"><strong>Actual Price</strong></p></td>
									<td align="right" colspan="2" style=" padding: 15px 7px; border-bottom: 1px solid #eee;">
										<p style="font-size: 13px; margin: 0; letter-spacing: 0.8px;">
											¢ {{ $order->actual_price }}
										</p>
									</td>
								</tr>
                                @if ($order->discounted_price > 0)
								<tr style="vertical-align: bottom;">
									<td colspan="1"></td>
									<td colspan="2" align="right"  style=" padding: 15px 7px; border-bottom: 1px solid #eee; "><p style="font-size: 13px; margin: 0; letter-spacing: 0.8px;"><strong>Discount<span>({{ $order->promo_code_discount_percent }}%)</span></strong></p></td>
									<td align="right" colspan="2" style=" padding: 15px 7px; border-bottom: 1px solid #eee;">
										<p style="font-size: 13px; margin: 0; letter-spacing: 0.8px;">¢ {{ $order->discounted_price }}</p>
									</td>
								</tr>
                                @endif
								<tr style="vertical-align: bottom;">
									<td colspan="1"></td>
									<td colspan="2" align="right"  style="padding: 15px 7px; border-top: 2px solid #f7941d; background-color: #fff; color: #f7941d; vertical-align: middle; font-weight: 700;"><p style="font-size: 13px; margin: 0; letter-spacing: 0.8px;"><strong>Grand Total</strong></p></td>
									<td align="right" colspan="2" style="padding: 15px 7px; border-top: 2px solid #f7941d; background-color: #fff; color: #f7941d; vertical-align: middle; font-weight: 700;">
										<p style="font-size: 13px; margin: 0; letter-spacing: 0.8px;">¢ {{ $order->total_amount }}</p>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>
