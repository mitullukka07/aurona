@extends('layouts_front.master')
@section('title','Aurona')
@section('content')
@include('layouts_front.service')
<div id="fh5co-product">
    <div class="container">
        <div class="row animate-box">
            <div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
                <span>Cool Stuff</span>
                <h2>Products.</h2>
                <p>Dignissimos asperiores vitae velit veniam totam fuga molestias accusamus alias autem provident. Odit ab aliquam dolor eius.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 text-center animate-box">
                <div class="product">
                    <div class="product-grid" style="background-image:url(front/images/product-1.jpg);">
                        <div class="inner">
                            <p>
                                <a href="{{ route('product_details') }}" class="icon"><i class="icon-shopping-cart"></i></a>
                                <a href="{{ route('product_details') }}" class="icon"><i class="icon-eye"></i></a>
                            </p>
                        </div>
                    </div>
                    <div class="desc">
                        <h3><a href="single.{{ route('product_details') }}">Hauteville Concrete Rocking Chair</a></h3>
                        <span class="price">$350</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 text-center animate-box">
                <div class="product">
                    <div class="product-grid" style="background-image:url(front/images/product-2.jpg);">
                        <span class="sale">Sale</span>
                        <div class="inner">
                            <p>
                                <a href="{{ route('product_details') }}" class="icon"><i class="icon-shopping-cart"></i></a>
                                <a href="{{ route('product_details') }}" class="icon"><i class="icon-eye"></i></a>
                            </p>
                        </div>
                    </div>
                    <div class="desc">
                        <h3><a href="single.{{ route('product_details') }}">Pavilion Speaker</a></h3>
                        <span class="price">$600</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 text-center animate-box">
                <div class="product">
                    <div class="product-grid" style="background-image:url(front/images/product-3.jpg);">
                        <div class="inner">
                            <p>
                                <a href="{{ route('product_details') }}" class="icon"><i class="icon-shopping-cart"></i></a>
                                <a href="{{ route('product_details') }}" class="icon"><i class="icon-eye"></i></a>
                            </p>
                        </div>
                    </div>
                    <div class="desc">
                        <h3><a href="single.{{ route('product_details') }}">Ligomancer</a></h3>
                        <span class="price">$780</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 text-center animate-box">
                <div class="product">
                    <div class="product-grid" style="background-image:url(front/images/product-4.jpg);">
                        <div class="inner">
                            <p>
                                <a href="{{ route('product_details') }}" class="icon"><i class="icon-shopping-cart"></i></a>
                                <a href="{{ route('product_details') }}" class="icon"><i class="icon-eye"></i></a>
                            </p>
                        </div>
                    </div>
                    <div class="desc">
                        <h3><a href="single.{{ route('product_details') }}">Alato Cabinet</a></h3>
                        <span class="price">$800</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 text-center animate-box">
                <div class="product">
                    <div class="product-grid" style="background-image:url(front/images/product-5.jpg);">
                        <div class="inner">
                            <p>
                                <a href="{{ route('product_details') }}" class="icon"><i class="icon-shopping-cart"></i></a>
                                <a href="{{ route('product_details') }}" class="icon"><i class="icon-eye"></i></a>
                            </p>
                        </div>
                    </div>
                    <div class="desc">
                        <h3><a href="single.{{ route('product_details') }}">Earing Wireless</a></h3>
                        <span class="price">$100</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 text-center animate-box">
                <div class="product">
                    <div class="product-grid" style="background-image:url(front/images/product-6.jpg);">
                        <div class="inner">
                            <p>
                                <a href="{{ route('product_details') }}" class="icon"><i class="icon-shopping-cart"></i></a>
                                <a href="{{ route('product_details') }}" class="icon"><i class="icon-eye"></i></a>
                            </p>
                        </div>
                    </div>
                    <div class="desc">
                        <h3><a href="single.{{ route('product_details') }}">Sculptural Coffee Table</a></h3>
                        <span class="price">$960</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('layouts_front.clients')
@include('layouts_front.testimonials')
@include('layouts_front.newslatter')
@endsection
