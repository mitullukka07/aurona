<!DOCTYPE html>
<html lang="en-us" class="no-js">
<head>
</head>

<body>
@if($data['isAndroidOS'])
<a href="intent:#Intent;scheme=mim://open?restaurant={!! $data['restaurant'] !!}&table={!! $data['table'] !!};package=com.mim;end" id="venderProfile"></a>
@else
<a href="itms-apps://apps.apple.com/us/mim-app/id1537096750" id="venderProfile"></a>
@endif
<script>
    document.getElementById("venderProfile").click();
</script>
</body>

</html>
