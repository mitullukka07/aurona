<div class="left side-menu">
    <button type="button" class="button-menu-mobile button-menu-mobile-topbar open-left waves-effect">
        <i class="ion-close"></i>
    </button>

    <div class="left-side-logo d-block d-lg-none">
        <div class="text-center">
            @isset($adminSetting['image'])
                <a href="{{ route('admin.dashboard') }}" class="logo"><img src="{{ asset('storage/'.$adminSetting['image']) }}" height="20" alt="logo"></a>
            @endisset
        </div>
    </div>

    <div class="sidebar-inner slimscrollleft">

        <div id="sidebar-menu">
            <ul>
                <li>
                    <a href="{{ route('admin.dashboard') }}" class="waves-effect">
                        <i class="dripicons-meter"></i>
                        <span> Dashboard </span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('admin.addon.index') }}" class="waves-effect">
                        <i class="fa fa-list"></i>
                        <span> Addon </span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('admin.category.index') }}" class="waves-effect">
                        <i class="fa fa-list"></i>
                        <span> Category </span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('admin.restaurants.index') }}" class="waves-effect">
                        <i class="fa fa-cutlery"></i>
                        <span> Restaurant </span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('admin.item.index') }}" class="waves-effect">
                        <i class="fa fa-list-alt"></i>
                        <span> Item </span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('admin.table.index') }}" class="waves-effect">
                        <i class="fa fa-table"></i>
                        <span> Table </span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('admin.order.index') }}" class="waves-effect">
                        <i class="fa fa-first-order"></i>
                        <span> Order </span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('admin.customers.index') }}" class="waves-effect">
                        <i class="fa fa-users"></i>
                        <span> Customer </span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('admin.promo-codes.index') }}" class="waves-effect">
                        <i class="fa fa-gift"></i>
                        <span> Promo Code </span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('admin.promo-codes.users') }}" class="waves-effect">
                        <i class="fa fa-ticket"></i>
                        <span> Promo Code Users </span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('admin.notification.index') }}" class="waves-effect">
                        <i class="fa fa-bell"></i>
                        <span> Notification </span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('admin.cms.index') }}" class="waves-effect">
                        <i class="fa fa-grav"></i>
                        <span> Cms </span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('admin.settings.index') }}" class="waves-effect">
                        <i class="fa fa-cog"></i>
                        <span> Setting </span>
                    </a>
                </li>

            </ul>
        </div>
        <div class="clearfix"></div>
    </div> <!-- end sidebarinner -->
</div>
