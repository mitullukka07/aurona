<!DOCTYPE HTML>
<html>
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>@yield('title')</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="gettemplates.co" />


	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<!-- <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet"> -->
	<!-- <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i" rel="stylesheet"> -->

	<!-- Animate.css -->
	<link rel="stylesheet" href="{{ asset('front/css/animate.css')}}">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="{{ asset('front/css/icomoon.css')}}">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="{{ asset('front/css/bootstrap.css')}}">

	<!-- Flexslider  -->
	<link rel="stylesheet" href="{{ asset('front/css/flexslider.css')}}">

	<!-- Owl Carousel  -->
	<link rel="stylesheet" href="{{ asset('front/css/owl.carousel.min.css')}}">
	<link rel="stylesheet" href="{{ asset('front/css/owl.theme.default.min.css')}}">

	<!-- Theme style  -->
	<link rel="stylesheet" href="{{ asset('front/css/style.css')}}">

	<!-- Modernizr JS -->
	<script src="{{ asset('front/js/modernizr-2.6.2.min.js')}}"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="{{ asset('front/js/respond.min.js')}}"></script>
	<![endif]-->

	</head>
	<body>

	<div class="fh5co-loader"></div>

	<div id="page">
	@include('layouts_front.menubar')

	@include('layouts_front.slider')

	@yield('content')

	@include('layouts_front.footer')
	</div>

	<div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
	</div>

	<!-- jQuery -->
	<script src="{{ asset('front/js/jquery.min.js')}}"></script>
	<!-- jQuery Easing -->
	<script src="{{ asset('front/js/jquery.easing.1.3.js')}}"></script>
	<!-- Bootstrap -->
	<script src="{{ asset('front/js/bootstrap.min.js')}}"></script>
	<!-- Waypoints -->
	<script src="{{ asset('front/js/jquery.waypoints.min.js')}}"></script>
	<!-- Carousel -->
	<script src="{{ asset('front/js/owl.carousel.min.js')}}"></script>
	<!-- countTo -->
	<script src="{{ asset('front/js/jquery.countTo.js')}}"></script>
	<!-- Flexslider -->
	<script src="{{ asset('front/js/jquery.flexslider-min.js')}}"></script>
	<!-- Main -->
	<script src="{{ asset('front/js/main.js')}}"></script>

	</body>
</html>

