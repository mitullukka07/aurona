@extends('layouts.master')

@section('page_title', 'CMS List')

@section('breadcrumb')
<div class="float-right page-breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
        <li class="breadcrumb-item active">CMS</li>
    </ol>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card m-b-30">
            <div class="card-header">
                <div class="card-header-actions">
                    <a href="{{ route('admin.cms.create') }}" class="btn btn-success float-right js_display_in_modal" title="Add category">Add Cms</a>
                </div>
            </div>
            <div class="card-body">
                @include('common.flash')

                <!-- ajax form response -->
                <div class="ajax-msg"></div>
                <div class="table-responsive">
                    {!! $dataTable->table(['class' =>  'table table-bordered table-hover dt-responsive']) !!}
                </div>
            </div>
        </div>
    </div>
</div>

    @include('modals.show')
@endsection

@push('page_scripts')

    {!! $dataTable->scripts() !!}
    <script type="text/javascript" src="{{ asset('assets\plugins\ckeditor\ckeditor.js')}}"></script>
    <script>
        function updateAllMessageForms(){
            for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
            }
        }

        $('body').on('submit', '#js_modal_form_custom', function(e){
            e.preventDefault();
            $('body').find('.has-danger').removeClass('has-danger');
            $(this).find('.error-msg-input').html("");
            if(jquery_validation($(this).find('.js_valid'))){
                updateAllMessageForms();
                my_modal    = $("#js_show_modal");
                url         = $(this).attr('action');
                form         = $(this);
                submit_form_ajax(form, my_modal, url);
            }
        });
    </script>

@endpush


