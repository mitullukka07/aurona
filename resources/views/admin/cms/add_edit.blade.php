<div class="modal-header bg-success">
<h5 class="modal-title" id="workerLabel">{{$data->id ? 'Edit' : 'Add'}} Record</h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body">
    {{ Form::model($data,array('route' => array('admin.cms.store'),'method' => 'post','id'=>"js_modal_form_custom",'enctype'=>"multipart/form-data",'novalidate'=>true)) }}
        {{Form::hidden('id')}}
        <div class="row">
            <div class="form-group col-md-6">
                {{Form::label('page','Page')}}

                {{Form::select('page',cmsPages(),null,
                    array(
                      'class'=>'form-control'
                    )
                )}}
                <span class="text-danger error-msg-input"></span>
            </div>
            <div class="form-group col-md-6">
                {{Form::label('title','Title')}}

                {{Form::text('title',null,
                            array(  'placeholder'   => 'Title in english',
                                    'class'         => 'form-control',
                                    'id'            => 'title')
                                    )}}
                <span class="text-danger error-msg-input"></span>
            </div>
            <div class="form-group col-md-12">
                {{Form::label('content','Content')}}

                {{Form::textarea('content',null,
                        array(  'placeholder'   => 'Content in english',
                                'class'         => 'form-control textarea',
                                'id'            => 'content',
                                'row'           =>  6)
                                )}}
                <span class="text-danger error-msg-input"></span>
            </div>
        </div>
        <div class="box-footer">
            <button type="reset" data-dismiss="modal" class="btn btn-default">Cancel</button>
            {{Form::button('Save',array('class'=>'btn btn-success pull-right','type'=>'submit'))}}
        </div>
</div>

<script type="text/javascript">
    CKEDITOR.replace('content');
    CKEDITOR.replace('content_it',{
        // contentsLangDirection: 'rtl'
    });
</script>
