<div class="modal-header bg-success">
    <h5 class="modal-title" id="workerLabel">Table Details</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <table class="table mb-0">
        <tr>
            <th>Name</th>
            <td>{{ $data->name }}</td>
        </tr>
        <tr>
            <th>Restaurant</th>
            <td>{{ optional($data->restaurant)->name }}</td>
        </tr>
        <tr>
            <th>Status</th>
            <td>
                @if ($data->is_active)
                    <span class="badge badge-success">Active</span>
                @else
                    <span class="badge badge-danger">In Active</span>
                @endif
            </td>
        </tr>
        <tr>
            <th>Created At</th>
            <td>{{ $data->created_at->format('F j, Y') }}</td>
        </tr>
        <tr>
            <th>Qr Code</th>
            <td><img src="{{ $data->qr_code }}" /></td>
        </tr>
    </table>
</div>
