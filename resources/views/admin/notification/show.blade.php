<div class="modal-header bg-success">
    <h5 class="modal-title" id="workerLabel">Cms Details</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <table class="table mb-0">
        <tr>
            <th>Title</th>
            <td>{{ $data->title }}</td>
            <th>Page</th>
            <td>{{ ucfirst(str_replace('_', ' ', $data->page)) }}</td>
        </tr>
        <tr>
            <td colspan="4">{!! $data->content !!}</td>
        </tr>
    </table>
</div>
