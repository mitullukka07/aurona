@extends('layouts.master')

@section('page_title', 'Notification List')

@section('breadcrumb')
<div class="float-right page-breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
        <li class="breadcrumb-item active">Notification</li>
    </ol>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card m-b-30">
            <div class="card-header">
                <div class="card-header-actions">
                </div>
            </div>
            <div class="card-body">
                @include('common.flash')

                <!-- ajax form response -->
                <div class="ajax-msg"></div>
                <div class="table-responsive">
                    {!! $dataTable->table(['class' =>  'table table-bordered table-hover dt-responsive']) !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('page_scripts')

    {!! $dataTable->scripts() !!}

@endpush


