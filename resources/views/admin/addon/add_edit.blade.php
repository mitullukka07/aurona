<div class="modal-header bg-success">
<h5 class="modal-title" id="workerLabel">{{$data->id ? 'Edit' : 'Add'}} Record</h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body">
    {{ Form::model($data,array('route' => array('admin.addon.store'),'method' => 'post','id'=>"js_modal_form",'enctype'=>"multipart/form-data",'novalidate'=>true)) }}
        {{Form::hidden('id')}}
        <div class="row">
            <div class="form-group col-md-6">
                {{Form::label('name','Name')}}

                {{Form::text('name',null,
                            array(  'placeholder'   => 'Name',
                                    'class'         => 'form-control',
                                    'id'            => 'name')
                                    )}}
                <span class="text-danger error-msg-input"></span>
            </div>
            <div class="form-group col-md-6">
                {{Form::label('price','Price')}}

                {{Form::number('price',null,
                            array(  'placeholder'   => 'Price',
                                    'class'         => 'form-control',
                                    'id'            => 'price',
                                    'min'           =>  1)
                                    )}}
                <span class="text-danger error-msg-input"></span>
            </div>
            <div class="form-group col-md-6">
                {{Form::label('description','Description')}}

                {{Form::textarea('description',null,
                        array(  'placeholder'   => 'Description',
                                'class'         => 'form-control textarea',
                                'id'            => 'description',
                                'rows'           =>  3)
                                )}}
                <span class="text-danger error-msg-input"></span>
            </div>
            <div class="form-group col-md-6">
                {{Form::label('is_active','Is Active?')}} <br>
                <label class="switch">
                    <input type="checkbox" name="is_active" class="form-control" {{ $data->is_active ? 'checked' : '' }}>
                    <span class="slider"></span>
                </label>
                <span class="text-danger error-msg-input"></span>
            </div>
        </div>
        <div class="box-footer">
            <button type="reset" data-dismiss="modal" class="btn btn-default">Cancel</button>
            {{Form::button('Save',array('class'=>'btn btn-success pull-right','type'=>'submit'))}}
        </div>
</div>
