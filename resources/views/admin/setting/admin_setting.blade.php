<div class="modal-header bg-success">
<h5 class="modal-title" id="workerLabel">{{isset($data['id']) ? 'Edit' : 'Add'}} Record</h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body">
    {{ Form::model($data,array('route' => array('admin.settings.store'),'method' => 'post','id'=>"js_modal_form_custom",'enctype'=>"multipart/form-data",'novalidate'=>true)) }}
        <input type="hidden" value="{{ isset($data['id']) ? $data['id'] : '' }}" name="id">

        <div class="row">
            <div class="form-group col-md-6">
                {{Form::label('name','Project Name')}}

                {{Form::text('name',null,
                        array(  'placeholder'   => 'Name',
                                'class'         => 'form-control js_valid required',
                                'id'            => 'name'
                                )
                                )}}
                <span class="text-danger error-msg-input"></span>
            </div>
            <div class="form-group col-md-6">
                {{Form::label('email','Email')}}

                {{Form::text('email',null,
                        array(  'placeholder'   => 'Email',
                                'class'         => 'form-control',
                                'id'            => 'email')
                                )}}
                <span class="text-danger error-msg-input"></span>
            </div>
            <div class="form-group col-md-6">
                {{Form::label('mobile','Mobile')}}

                {{Form::text('mobile',null,
                        array(  'placeholder'   => 'Mobile',
                                'class'         => 'form-control',
                                'id'            => 'mobile')
                                )}}
                <span class="text-danger error-msg-input"></span>
            </div>
            <div class="form-group col-md-4">
                {{Form::label('image','Image')}}

                {{Form::file('image',
                        array(  'placeholder'   => 'Image',
                                'class'         => 'form-control',
                                'id'            => 'image')
                                )}}
                <span class="text-danger error-msg-input"></span>
            </div>
            <div class="form-group col-md-2">
                @if(isset($data['image']))
                <input type="hidden" value="{{ $data['image'] }}" name="old_image" />
                <img src="{{ asset('storage'.'/'.$data['image']) }}" height="80px" width="80px" />
                @endif
            </div>
            <div class="form-group col-md-6">
                {{Form::label('address','Address')}}

                {{Form::textarea('address',null,
                        array(  'placeholder'   => 'Address',
                                'class'         => 'form-control',
                                'id'            => 'address',
                                'rows'          => 4)
                                )}}
                <span class="text-danger error-msg-input"></span>
            </div>
            <div class="form-group col-md-6">
                {{Form::label('description','Description')}}

                {{Form::textarea('description',null,
                        array(  'placeholder'   => 'Description',
                                'class'         => 'form-control',
                                'id'            => 'description',
                                'rows'          => 4)
                                )}}
                <span class="text-danger error-msg-input"></span>
            </div>
        </div>
        <div class="box-footer">
            <button type="reset" data-dismiss="modal" class="btn btn-default">Cancel</button>
            {{Form::button('Save',array('class'=>'btn btn-success pull-right','type'=>'submit'))}}
        </div>
</div>
