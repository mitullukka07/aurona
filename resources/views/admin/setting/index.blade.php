@extends('layouts.master')

@section('page_title', 'Settings')

@section('breadcrumb')
<div class="float-right page-breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
        <li class="breadcrumb-item active">Setting</li>
    </ol>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        @include('common.flash')
    </div>
    <div class="col-xl-6">
        <div class="card m-b-30">
            <div class="card-header-flex bg-success">
                <h6 class="text-uppercase mb-0 mt-0">Admin Setting</h6>
                @if ($admin_setting)
                    <a href="{{ route('admin.settings.edit',$admin_setting->id) }}" class="btn btn-outline-light float-right js_display_in_modal" title="Add category">Edit</a>
                @else
                    <a href="{{ route('admin.settings.create') }}" class="btn btn-outline-light float-right js_display_in_modal" title="Add category">Add</a>
                @endif
            </div>
            @if($admin_setting)
            @php
                $data = $admin_setting->value;
            @endphp
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col-md-8">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item"><b>Name</b> : {{ isset($data['name']) ? $data['name'] : '' }}</li>
                            <li class="list-group-item"><b>Email</b> : {{ isset($data['email']) ? $data['email'] : '' }}</li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <div class="text-center">
                            @isset($data['image'])
                                <img src="{{ asset('storage'.'/'.$data['image']) }}" height="80px" width="80px" />
                            @endisset
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item"></li>
                            <li class="list-group-item"><b>Address</b> : {{ isset($data['address']) ? $data['address'] : '' }}</li>
                            <li class="list-group-item"><b>Description</b> : {{ isset($data['description']) ? $data['description'] : '' }}</li>
                        </ul>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
    @include('modals.show')
@endsection
@push('page_scripts')
    <script>
        $('body').on('submit', '#js_modal_form_custom', function(e){
            e.preventDefault();
            $('body').find('.has-danger').removeClass('has-danger');
            $(this).find('.error-msg-input').html("");
            if(jquery_validation($(this).find('.js_valid'))){
                my_modal    = $("#js_show_modal");
                url         = $(this).attr('action');
                form        = $(this);
                $.ajax({
                    url: url,
                    type: 'post',
                    processData: false,
                    contentType: false,
                    // data : new FormData(form),
                    data : new FormData(form[0]),
                    success: function(result){
                        if(result) {
                            $html = '<div class="alert alert-block alert-'+result.status+'"><button type="button" class="close" data-dismiss="alert">×</button><strong>'+result.message+'</strong></div>';
                            $('.ajax-msg').append($html);
                        }
                        my_modal.modal('hide');
                        location.reload();
                    },
                    complete:function(){
                        window.setTimeout(function() {
                            $(".alert").fadeTo(500, 0).slideUp(500, function(){
                                $(this).remove();
                            });
                        }, 3000);
                    },
                    error: function (data) {
                        if(data.status === 422) {
                            var errors = $.parseJSON(data.responseText);
                            $.each(errors.errors, function (key, value) {
                                my_modal.find('#'+key).parents('.form-group').addClass('has-danger');
                                my_modal.find('#'+key).parents('.form-group')
                                .find('.error-msg-input').html(value);
                            });
                        }
                    }
                });
            }
        });
    </script>

@endpush


