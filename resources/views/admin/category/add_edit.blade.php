<div class="modal-header bg-success">
    <h5 class="modal-title" id="workerLabel">{{$data->id ? 'Edit' : 'Add'}} Record</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    </div>
    <div class="modal-body">
        {{ Form::model($data,array('route' => array('admin.category.store'),'method' => 'post','id'=>"js_modal_form",'enctype'=>"multipart/form-data",'novalidate'=>true)) }}
            {{Form::hidden('id')}}
            <div class="row">
                <div class="form-group col-md-12">
                    {{Form::label('name','Name')}}

                    {{Form::text('name',null,
                            array(  'placeholder'   => 'Name',
                                    'class'         => 'form-control js_valid required',
                                    'id'            => 'name'
                                    )
                                    )}}
                    <span class="text-danger error-msg-input"></span>
                </div>
                <div class="form-group col-md-12 w-100">
                    {{Form::label('addon_ids','Addon')}}

                    {{Form::select('addon_ids[]',$addons,null,
                        array(
                            'class'       => 'form-control select2',
                            'multiple'    => "multiple",
                        )
                    )}}
                    <span class="text-danger error-msg-input"></span>
                </div>
                <div class="form-group col-md-6">
                    {{Form::label('image','Image')}}

                    {{Form::file('image',
                            array(  'placeholder'   => 'Image',
                                    'class'         => 'form-control',
                                    'id'            => 'image')
                                    )}}
                    <span class="text-danger error-msg-input"></span>
                </div>
                <div class="form-group col-md-6">
                    @if(isset($data->image) && !empty($data->image))
                    <img src="{{ $data->image }}" height="80px" width="80px" />
                    @endif
                </div>
            </div>
            <div class="box-footer">
                <button type="reset" data-dismiss="modal" class="btn btn-default">Cancel</button>
                {{Form::button('Save',array('class'=>'btn btn-success pull-right','type'=>'submit'))}}
            </div>
    </div>
    <script>
        $('.select2').select2({placeholder: "Select addons",allowClear: true});
    </script>
