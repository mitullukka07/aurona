<div class="modal-header bg-success">
    <h5 class="modal-title" id="workerLabel">Item Details</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <table class="table mb-0">
        <tr>
            <th>Name</th>
            <td>{{ $data->name }}</td>
            <th>Price</th>
            <td>{{ $data->price }}</td>
        </tr>
        <tr>
            <th>Category</th>
            <td>{{ optional($data->category)->name }}</td>
            <th>Restaurant</th>
            <td>{{ optional($data->restaurant)->name }}</td>
        </tr>
        <tr>
            <th>Status</th>
            <td>
                @if ($data->is_active)
                    <span class="badge badge-success">Active</span>
                @else
                    <span class="badge badge-danger">In Active</span>
                @endif
            </td>
            <th>Description</th>
            <td>{{ $data->description }}</td>
        </tr>
        <tr>
            <th>Image</th>
            <td>
                <a href="{{ $data->images }}" target="_blank"><img src="{{ $data->images }}" width="50px" height="50px" /></a>
            </td>
            <th>Created At</th>
            <td colspan="3">{{ $data->created_at->format('F j, Y') }}</td>
        </tr>
    </table>
</div>
