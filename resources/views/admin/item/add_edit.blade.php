<div class="modal-header bg-success">
<h5 class="modal-title" id="workerLabel">{{$data->id ? 'Edit' : 'Add'}} Record</h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body">
    {{ Form::model($data,array('route' => array('admin.item.store'),'method' => 'post','id'=>"js_modal_form",'enctype'=>"multipart/form-data",'novalidate'=>true)) }}
        {{Form::hidden('id')}}
        <div class="row">
            <div class="form-group col-md-6">
                {{Form::label('name','Name')}}

                {{Form::text('name',null,
                        array(  'placeholder'   => 'Name',
                                'class'         => 'form-control js_valid required',
                                'id'            => 'name'
                                )
                                )}}
                <span class="text-danger error-msg-input"></span>
            </div>
            <div class="form-group col-md-6">
                {{Form::label('price','Price')}}

                {{Form::number('price',null,
                            array(  'placeholder'   => 'Price',
                                    'class'         => 'form-control',
                                    'id'            => 'price',
                                    'min'           =>  1)
                                    )}}
                <span class="text-danger error-msg-input"></span>
            </div>
            <div class="form-group col-md-6 w-100">
                {{Form::label('restaurant_id','Restaurant')}}

                {{Form::select('restaurant_id',$restaurants,null,
                    array(
                        'class'       => 'form-control select2',
                        'id'          => 'restaurant_id',
                        'placeholder' => 'Select Restaurant'
                    )
                )}}
                <span class="text-danger error-msg-input"></span>
            </div>
            <div class="form-group col-md-6 w-100">
                {{Form::label('category_id','Category')}}
                <div id="js_category_select">
                    {{Form::select('category_id',$category,null,
                        array(
                            'class'       => 'form-control select2',
                            'id'          => 'category_id',
                            'placeholder' => 'Select Category'
                        )
                    )}}
                </div>
                <span class="text-danger error-msg-input"></span>
            </div>
            <div class="form-group col-md-6">
                {{Form::label('images','Item Image')}}

                {{Form::file('images',
                        array(  'placeholder'   => 'Item Image',
                                'class'         => 'form-control',
                                'id'            => 'images')
                                )}}
                <span class="text-danger error-msg-input"></span>
            </div>
            <div class="form-group col-md-6">
                {{Form::label('is_active','Is Active?')}} <br>
                <label class="switch">
                    <input type="checkbox" name="is_active" class="form-control" {{ $data->is_active ? 'checked' : '' }}>
                    <span class="slider"></span>
                </label>
                <span class="text-danger error-msg-input"></span>
            </div>
            <div class="form-group col-md-12">
                {{Form::label('description','Description')}}

                {{Form::textarea('description',null,
                        array(  'placeholder'   => 'Description',
                                'class'         => 'form-control textarea',
                                'id'            => 'description',
                                'rows'           =>  3)
                                )}}
                <span class="text-danger error-msg-input"></span>
            </div>
        </div>
        <div class="box-footer">
            <button type="reset" data-dismiss="modal" class="btn btn-default">Cancel</button>
            {{Form::button('Save',array('class'=>'btn btn-success pull-right','type'=>'submit'))}}
        </div>
</div>
<script>
    $('.select2').select2();
</script>


