{{Form::select('category_id',$category,null,
    array(
        'class'       => 'form-control select2',
        'id'          => 'category_id',
        'placeholder' => 'Select Category'
    )
)}}
<script>
    $('.select2').select2();
</script>
