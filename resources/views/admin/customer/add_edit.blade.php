<div class="modal-header bg-success">
<h5 class="modal-title" id="workerLabel">{{$data->id ? 'Edit' : 'Add'}} Record</h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body">
    {{ Form::model($data,array('route' => array('admin.customers.store'),'method' => 'post','id'=>"js_modal_form",'enctype'=>"multipart/form-data",'novalidate'=>true)) }}
        {{Form::hidden('id')}}
        <div class="row">
            <div class="form-group col-md-6">
                {{Form::label('name','Name')}}

                {{Form::text('name',null,
                        array(  'placeholder'   => 'Name',
                                'class'         => 'form-control js_valid required',
                                'id'            => 'name'
                                )
                                )}}
                    <span class="text-danger error-msg-input"></span>
            </div>
            <div class="form-group col-md-6">
                {{Form::label('email','Email')}}

                {{Form::text('email',null,
                        array(  'placeholder'   => 'Email',
                                'class'         => 'form-control',
                                'id'            => 'email')
                                )}}
                    <span class="text-danger error-msg-input"></span>
            </div>
            <div class="form-group col-md-6">
                {{Form::label('phone','Phone')}}

                {{Form::text('phone',null,
                        array(  'placeholder'   => 'Phone',
                                'class'         => 'form-control',
                                'id'            => 'phone')
                                )}}
                    <span class="text-danger error-msg-input"></span>
            </div>
        </div>
        <div class="box-footer">
            <button type="reset" data-dismiss="modal" class="btn btn-default">Cancel</button>
            {{Form::button('Save',array('class'=>'btn btn-success pull-right','type'=>'submit'))}}
        </div>
</div>
