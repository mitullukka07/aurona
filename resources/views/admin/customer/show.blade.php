<div class="modal-header bg-success">
    <h5 class="modal-title" id="workerLabel">Customer Details</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <table class="table mb-0">
        <tr>
            <th>Name</th>
            <td>{{ $data->name }}</td>
            <th>Email</th>
            <td>{{ $data->email }}</td>
        </tr>
        <tr>
            <th>Phone</th>
            <td>{{ $data->phone }}</td>
            <th>Created At</th>
            <td>{{ $data->created_at->format('F j, Y') }}</td>
        </tr>
    </table>
</div>
