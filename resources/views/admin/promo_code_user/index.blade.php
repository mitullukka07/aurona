@extends('layouts.master')

@section('page_title', 'Promo Code User List')

@section('breadcrumb')
<div class="float-right page-breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
        <li class="breadcrumb-item active">Promo Code Users</li>
    </ol>
</div>
@endsection
@push('page_css')
<style>
</style>
@endpush

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card m-b-30">
            <div class="card-header">
                <div class="card-header-actions">
                </div>
            </div>
            <div class="card-body">
                @include('common.flash')

                <!-- ajax form response -->
                <div class="ajax-msg"></div>
                <div class="table-responsive">
                    {!! $dataTable->table(['class' =>  'table table-bordered table-hover dt-responsive']) !!}
                </div>
            </div>
        </div>
    </div>
</div>

    @include('modals.show')
@endsection

@push('page_scripts')

    {!! $dataTable->scripts() !!}

    <script>
        $(document).on('click', '.js_delete_custom', function() {
        var url             = $(this).attr('url');
        var promocode_id    = $(this).attr('promocode_id');
        var user_id         = $(this).attr('user_id');
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!',
            reverseButtons: true
        }).then((result) => {
            if (result) {
                $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url : url,
                        type : 'PATCH',
                        data : {'promocode_id' : promocode_id, 'user_id' : user_id},
                    success:function(res) {
                        if (res) {
                            swal(
                                res.action, //get from controller (block/unblock/cancel)
                                res.msg, // get from controller
                                res.status // get from controller (success/error)
                            )
                            window.LaravelDataTables[res.datatable_id].draw();
                        }

                    }
                });
            } else {
                swal("Cancelled", "Status not changed :)", "error");
            }
        }).catch((error) => {
            $('.js_delete_custom').attr('disabled', false);
        });
    });
    </script>

@endpush


