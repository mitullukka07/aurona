<div class="modal-header bg-success">
    <h5 class="modal-title" id="workerLabel">Restraunt Details</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <table class="table mb-0">
        <tr>
            <th>Name</th>
            <td>{{ $data->name }}</td>
            <th>Email</th>
            <td>{{ $data->email }}</td>
        </tr>
        <tr>
            <th>Mobile</th>
            <td>{{ $data->mobile }}</td>
            <th>Image</th>
            <td><img src="{{ $data->image }}" width="50px" height="50px" /></td>
        </tr>
        <tr>
            <th>Category</th>
            <td colspan="3">
                @foreach (getRestaurantCategory($data->category_ids) as $category)
                <span class="badge badge-success">{{ $category->name }}</span>
                @endforeach
            </td>
        </tr>
        <tr>
            <th>Address</th>
            <td>{{ $data->address }}</td>
            <th>Description</th>
            <td>{{ $data->description }}</td>
        </tr>
        <tr>
            <th>Restaurant Time</th>
            <td>{{ $data->restaurant_time }}</td>
            <th>Created At</th>
            <td>{{ $data->created_at->format('F j, Y') }}</td>
        </tr>
    </table>
</div>
