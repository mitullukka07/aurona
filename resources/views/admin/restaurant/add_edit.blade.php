<div class="modal-header bg-success">
<h5 class="modal-title" id="workerLabel">{{$data->id ? 'Edit' : 'Add'}} Record</h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body">
    {{ Form::model($data,array('route' => array('admin.restaurants.store'),'method' => 'post','id'=>"js_modal_form",'enctype'=>"multipart/form-data",'novalidate'=>true)) }}
        {{Form::hidden('id')}}
        <div class="row">
            <div class="form-group col-md-6">
                {{Form::label('name','Name')}}

                {{Form::text('name',null,
                        array(  'placeholder'   => 'Name',
                                'class'         => 'form-control js_valid required',
                                'id'            => 'name'
                                )
                                )}}
                <span class="text-danger error-msg-input"></span>
            </div>
            <div class="form-group col-md-6">
                {{Form::label('email','Email')}}

                {{Form::text('email',null,
                        array(  'placeholder'   => 'Email',
                                'class'         => 'form-control',
                                'id'            => 'email')
                                )}}
                <span class="text-danger error-msg-input"></span>
            </div>
            <div class="form-group col-md-6">
                {{Form::label('mobile','Mobile')}}

                {{Form::text('mobile',null,
                        array(  'placeholder'   => 'Mobile',
                                'class'         => 'form-control',
                                'id'            => 'mobile')
                                )}}
                <span class="text-danger error-msg-input"></span>
            </div>
            <div class="form-group col-md-6">
                {{Form::label('image','Image')}}

                {{Form::file('image',
                        array(  'placeholder'   => 'Image',
                                'class'         => 'form-control',
                                'id'            => 'image')
                                )}}
                <span class="text-danger error-msg-input"></span>
            </div>
            <div class="form-group col-md-12 w-100">
                {{Form::label('addon_ids','Addon')}}
                {{Form::select('category_ids[]',$category,null,
                    array(
                        'class'       => 'form-control select2',
                        'multiple'    => "multiple",
                        'id'          => "category_ids",
                    )
                )}}
                <span class="text-danger error-msg-input"></span>
            </div>
            <div class="form-group col-md-6">
                {{Form::label('description','Description')}}

                {{Form::textarea('description',null,
                        array(  'placeholder'   => 'Description',
                                'class'         => 'form-control',
                                'id'            => 'description',
                                'rows'          => 4)
                                )}}
                <span class="text-danger error-msg-input"></span>
            </div>
            <div class="form-group col-md-6">
                {{Form::label('address','Address')}}

                {{Form::textarea('address',null,
                        array(  'placeholder'   => 'Address',
                                'class'         => 'form-control',
                                'id'            => 'address',
                                'rows'          => 4)
                                )}}
                <span class="text-danger error-msg-input"></span>
            </div>
            <div class="form-group col-md-6">
                {{Form::label('lat','Latitude')}}

                {{Form::number('lat',null,
                        array(  'placeholder'   => 'Latitude',
                                'class'         => 'form-control',
                                'id'            => 'lat',
                                'min'           =>  1)
                                )}}
                <span class="text-danger error-msg-input"></span>
            </div>
            <div class="form-group col-md-6">
                {{Form::label('long','Longitude')}}


                {{Form::text('long',null,
                        array(  'placeholder'   => 'Longitude',
                                'class'         => 'form-control',
                                'id'            => 'long',
                                'min'           =>  1)
                                )}}
                <span class="text-danger error-msg-input"></span>
            </div>
            <div class="form-group col-md-12">
                {{Form::label('restaurant_time','Restaurant Time')}}

                {{Form::text('restaurant_time',null,
                        array(  'placeholder'   => 'Restaurant Time',
                                'class'         => 'form-control',
                                'id'            => 'restaurant_time',
                                'rows'          => 4)
                                )}}
                <span class="text-danger error-msg-input"></span>
            </div>
        </div>
        <div class="box-footer">
            <button type="reset" data-dismiss="modal" class="btn btn-default">Cancel</button>
            {{Form::button('Save',array('class'=>'btn btn-success pull-right','type'=>'submit'))}}
        </div>
</div>
<script>
    $('.select2').select2({placeholder: "Select Category",allowClear: true});
</script>
