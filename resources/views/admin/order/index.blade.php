@extends('layouts.master')

@section('page_title', 'Order List')

@section('breadcrumb')
<div class="float-right page-breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
        <li class="breadcrumb-item active">Order</li>
    </ol>
</div>
@endsection
@push('page_css')
@endpush

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card m-b-30">
            <div class="card-header">
                <div class="card-header-actions">

                </div>
            </div>
            <div class="card-body">
                @include('common.flash')

                <!-- ajax form response -->
                <div class="ajax-msg"></div>
                <div class="table-responsive">
                    {!! $dataTable->table(['class' =>  'table table-bordered table-hover dt-responsive']) !!}
                </div>
            </div>
        </div>
    </div>
</div>
    @include('modals.show')
@endsection

@push('page_scripts')
    {!! $dataTable->scripts() !!}
    <script>
        $(document).on('click', '.changeOrderStatus', function() {
            var this_var = $(this);
            var data_id = $(this).attr('data-id');
            var url     = $(this).attr('data-url');
            swal({
                title: 'Are you sure want to change as completed order ?',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Do It!',
                reverseButtons: true
                }).then((result) => {
                    if (result) {
                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            url : url,
                            type : 'PATCH',
                            data : {'data_id' : data_id},
                            success : function (res) {
                                swal(
                                    res.action, //get from controller (block/unblock/cancel)
                                    res.msg, // get from controller
                                    res.status // get from controller (success/error)
                                )
                                window.LaravelDataTables[res.datatable_id].draw();

                            }
                        });
                    } else {
                        swal("Completed", "Order Not Completed :)", "error");
                        this_var.prop('checked', false);
                    }
                }).catch((error) => {
                this_var.prop('checked', false);
            });

        });

        $(document).on('change', '.changeItemStatus', function() {
            var this_var    = $(this);
            var data_id     = $(this).attr('data-id');
            var url         = $(this).attr('data-url');
            swal({
                title: 'Is item ready to take ?',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Do It!',
                reverseButtons: true
                }).then((result) => {
                    if (result) {
                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            url : url,
                            type : 'PATCH',
                            data : {'data_id' : data_id},
                            success : function (res) {
                                console.log(res);
                                swal(
                                    res.action, //get from controller (block/unblock/cancel)
                                    res.msg, // get from controller
                                    res.status // get from controller (success/error)
                                )
                                if (res.status == 'success') {
                                    this_var.parents('td').html('<span class="badge badge-success">Ready To Take</span>');
                                }
                            }
                        });
                    } else {
                        swal("Completed", "Order Not Completed :)", "error");
                        this_var.prop('checked', false);
                    }
            }).catch((error) => {
                this_var.prop('checked', false);
            });

        });
    </script>
@endpush


