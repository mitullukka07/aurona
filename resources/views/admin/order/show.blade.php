<div class="modal-header bg-success">
    <h5 class="modal-title" id="workerLabel">Order Details</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-12">
            <div class="card m-b-30">
                <div class="card-body">
                    <div class="row">
                        <div class="col-6">
                            <address>
                                <strong>Customer Details:</strong><br>
                                {{ optional($data->user)->name }},<br>
                                {{ optional($data->user)->email }},<br>
                                {{ optional($data->user)->address }}<br>
                                {{ optional($data->user)->mobile }}
                            </address>
                        </div>
                        <div class="col-6 text-right">
                            <address>
                                <strong>Restaurant Details:</strong><br>
                                {{ optional($data->restaurant)->name }},<br>
                                {{ optional($data->restaurant)->email }},<br>
                                {{ optional($data->restaurant)->mobile }},<br>
                                {{ optional($data->restaurant)->address }}
                            </address>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6 m-t-30">
                            <address>
                                <strong>Payment Method:</strong> {{ $data->payment_type == 1 ? 'Cash' : 'Card' }}<br>
                            </address>
                        </div>
                        <div class="col-6 m-t-30 text-right">
                            <address>
                                <strong>Order Date:</strong> {{ $data->created_at->format('F j, Y') }}<br>
                                <strong>Table:</strong> {{ optional($data->table)->name }}
                            </address>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <div class="panel panel-default">
                                <div class="p-2">
                                    <h3 class="panel-title font-20"><strong>Order summary</strong></h3>
                                </div>
                                <div class="">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <td><strong>Item</strong></td>
                                                <td class="text-center"><strong>Item Price</strong></td>
                                                <td class="text-center"><strong>Addon Price</strong></td>
                                                <td class="text-center"><strong>Quantity</strong></td>
                                                <td class="text-center"><strong>Item Status</strong></td>
                                                <td class="text-right"><strong>Totals</strong></td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach ($data->order_details as $detail)
                                                <tr>
                                                    <td>{{ optional( $detail->item)->name }}</td>
                                                    <td class="text-center">¢ {{ $detail->item_price }}</td>
                                                    <td class="text-center">¢ {{ $detail->addon_price }}</td>
                                                    <td class="text-center">{{ $detail->quantity }}</td>
                                                    <td class="text-center">
                                                        @if ($detail->item_status == 1)
                                                        <label class="switch" title="Click Ready to take item status">
                                                            <input type="checkbox" name="is_active" class="form-control changeItemStatus" data-url="{{ route('admin.order.ready_to_take') }}" data-id="{{ $detail->id }}">
                                                            <span class="slider"></span>
                                                        </label>
                                                        @else
                                                        <span class="badge badge-success">Ready To Take</span>
                                                        @endif
                                                    </td>
                                                    <td class="text-right">¢ {{ $detail->total_price }}</td>
                                                </tr>
                                            @endforeach
                                            <tr>
                                                <td colspan="4" class="thick-line"></td>
                                                <td class="thick-line text-right">
                                                    <strong>Actual Price</strong></td>
                                                <td class="thick-line text-right">¢ {{ $data->actual_price }}</td>
                                            </tr>
                                            @if ($data->discounted_price > 0)
                                            <tr>
                                                <td colspan="4" class="no-line"></td>
                                                <td class="no-line text-right">
                                                    <strong>Promo Code</strong></td>
                                                <td class="no-line text-right">{{ $data->promo_code }}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" class="no-line"></td>
                                                <td class="no-line text-right">
                                                    <strong>Discount Percent</strong></td>
                                                <td class="no-line text-right">{{ $data->promo_code_discount_percent }} %</td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" class="no-line"></td>
                                                <td class="no-line text-right">
                                                    <strong>Discount Price</strong></td>
                                                <td class="no-line text-right">¢ {{ $data->discounted_price }}</td>
                                            </tr>
                                            @endif
                                            <tr>
                                                <td colspan="4" class="no-line"></td>
                                                <td class="no-line text-right">
                                                    <strong>Total Amount</strong></td>
                                                <td class="no-line text-right">¢ {{ $data->total_amount }}</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                    {{-- <div class="d-print-none mo-mt-2">
                                        <div class="float-right">
                                            <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light"><i class="fa fa-print"></i></a>
                                            <a href="#" class="btn btn-primary waves-effect waves-light">Send</a>
                                        </div>
                                    </div> --}}
                                </div>
                            </div>

                        </div>
                    </div> <!-- end row -->

                </div>
            </div>
        </div> <!-- end col -->
    </div>
</div>
