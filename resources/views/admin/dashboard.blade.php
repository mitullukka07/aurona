@extends('layouts.master')

@section('page_title', 'Dashboard')

@section('page_head')
<div class="float-right page-breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Admin</a></li>
        <li class="breadcrumb-item active">Dashboard</li>
    </ol>
</div>
@endsection

@section('content')

    <div class="row">
        <div class="col-xl-3 col-md-6">
            <div class="card mini-stat m-b-30">
                <div class="p-3 bg-green text-white">
                    <div class="mini-stat-icon">
                        <i class="fa fa-users float-right mb-0"></i>
                    </div>
                    <h6 class="text-uppercase mb-0">Total Customer</h6>
                </div>
                <div class="card-body">
                    <div class="border-bottom pb-4">
                            <span class="badge badge-success"> +22% </span> <span class="ml-2 text-muted">From previous period</span>
                    </div>
                    <div class="mt-4 text-muted">
                        <div class="float-right">
                            <p class="m-0">Last : 3426</p>
                        </div>
                        <h5 class="m-0">{{ $total_customer }}<i class="mdi mdi-arrow-up text-success ml-2"></i></h5>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card mini-stat m-b-30">
                <div class="p-3 bg-green text-white">
                    <div class="mini-stat-icon">
                        <i class="fa fa-cutlery float-right mb-0"></i>
                    </div>
                    <h6 class="text-uppercase mb-0">Total Restaurant</h6>
                </div>
                <div class="card-body">
                    <div class="border-bottom pb-4">
                            <span class="badge badge-success"> +22% </span> <span class="ml-2 text-muted">From previous period</span>
                    </div>
                    <div class="mt-4 text-muted">
                        <div class="float-right">
                            <p class="m-0">Last : 3426</p>
                        </div>
                        <h5 class="m-0">{{ $total_restaurant }}<i class="mdi mdi-arrow-up text-success ml-2"></i></h5>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card mini-stat m-b-30">
                <div class="p-3 bg-green text-white">
                    <div class="mini-stat-icon">
                        <i class="fa fa-shopping-bag float-right mb-0"></i>
                    </div>
                    <h6 class="text-uppercase mb-0">Total Order</h6>
                </div>
                <div class="card-body">
                    <div class="border-bottom pb-4">
                            <span class="badge badge-success"> +22% </span> <span class="ml-2 text-muted">From previous period</span>
                    </div>
                    <div class="mt-4 text-muted">
                        <div class="float-right">
                            <p class="m-0">Last : 3426</p>
                        </div>
                        <h5 class="m-0">{{ $total_customer }}<i class="mdi mdi-arrow-up text-success ml-2"></i></h5>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card mini-stat m-b-30">
                <div class="p-3 bg-green text-white">
                    <div class="mini-stat-icon">
                        <i class="fa fa-money float-right mb-0"></i>
                    </div>
                    <h6 class="text-uppercase mb-0">Total Income</h6>
                </div>
                <div class="card-body">
                    <div class="border-bottom pb-4">
                            <span class="badge badge-success"> +22% </span> <span class="ml-2 text-muted">From previous period</span>
                    </div>
                    <div class="mt-4 text-muted">
                        <div class="float-right">
                            <p class="m-0">Last : 3426</p>
                        </div>
                        <h5 class="m-0">{{ $total_customer }}<i class="mdi mdi-arrow-up text-success ml-2"></i></h5>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xl-8">
            <div class="card m-b-30">
                <div class="card-body">
                    <h4 class="mt-0 header-title">Sales</h4>

                    <ul class="list-inline widget-chart m-t-20 text-center">
                    <li>
                    <h4 class=""><b>3652</b></h4>
                    <p class="text-muted m-b-0">Expenses</p>
                    </li>
                    <li>
                    <h4 class=""><b>5421</b></h4>
                    <p class="text-muted m-b-0">Last week</p>
                    </li>
                    <li>
                    <h4 class=""><b>9652</b></h4>
                    <p class="text-muted m-b-0">Last Month</p>
                    </li>
                    </ul>

                    <div id="morris-area-example" style="height: 300px; position: relative; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"><svg height="300" version="1.1" width="649.328" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="overflow: hidden; position: relative;"><desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.1.2</desc><defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs><text x="32.859375" y="261" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">0</tspan></text><path fill="none" stroke="#eeeeee" d="M45.359375,261H624.328" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="32.859375" y="202" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">100</tspan></text><path fill="none" stroke="#eeeeee" d="M45.359375,202H624.328" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="32.859375" y="143" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">200</tspan></text><path fill="none" stroke="#eeeeee" d="M45.359375,143H624.328" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="32.859375" y="84" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">300</tspan></text><path fill="none" stroke="#eeeeee" d="M45.359375,84H624.328" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="32.859375" y="25" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">400</tspan></text><path fill="none" stroke="#eeeeee" d="M45.359375,25H624.328" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="624.328" y="273.5" text-anchor="middle" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2018</tspan></text><text x="527.9212718407846" y="273.5" text-anchor="middle" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2017</tspan></text><text x="431.2504156592153" y="273.5" text-anchor="middle" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2016</tspan></text><text x="334.8436875" y="273.5" text-anchor="middle" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2015</tspan></text><text x="238.43695934078465" y="273.5" text-anchor="middle" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2014</tspan></text><text x="142.03023118156932" y="273.5" text-anchor="middle" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2013</tspan></text><text x="45.359375" y="273.5" text-anchor="middle" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2012</tspan></text><path fill="#f99ea4" stroke="none" d="M45.359375,261C69.52708904539233,238.875,117.86251713617699,176.93105335157318,142.03023118156932,172.5C166.13191322137317,168.0810533515732,214.3352773009808,227.8125,238.43695934078465,225.6C262.5386413805885,223.3875,310.74200546019614,157.01250000000002,334.8436875,154.8C358.94536953980383,152.5875,407.1487336194115,200.16684336525307,431.2504156592153,207.9C455.4181297046076,215.65434336525308,503.7535577953923,212.31894664842682,527.9212718407846,216.75C552.0229538805884,221.1689466484268,600.2263179601962,236.66250000000002,624.328,243.3L624.328,261L45.359375,261Z" fill-opacity="0.7" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 0.7;"></path><path fill="none" stroke="#ff5560" d="M45.359375,261C69.52708904539233,238.875,117.86251713617699,176.93105335157318,142.03023118156932,172.5C166.13191322137317,168.0810533515732,214.3352773009808,227.8125,238.43695934078465,225.6C262.5386413805885,223.3875,310.74200546019614,157.01250000000002,334.8436875,154.8C358.94536953980383,152.5875,407.1487336194115,200.16684336525307,431.2504156592153,207.9C455.4181297046076,215.65434336525308,503.7535577953923,212.31894664842682,527.9212718407846,216.75C552.0229538805884,221.1689466484268,600.2263179601962,236.66250000000002,624.328,243.3" stroke-width="0" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><circle cx="45.359375" cy="261" r="0" fill="#ff5560" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="142.03023118156932" cy="172.5" r="0" fill="#ff5560" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="238.43695934078465" cy="225.6" r="0" fill="#ff5560" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="334.8436875" cy="154.8" r="0" fill="#ff5560" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="431.2504156592153" cy="207.9" r="0" fill="#ff5560" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="527.9212718407846" cy="216.75" r="0" fill="#ff5560" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="624.328" cy="243.3" r="0" fill="#ff5560" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><path fill="#f7d692" stroke="none" d="M45.359375,261C69.52708904539233,254.3625,117.86251713617699,245.52763337893296,142.03023118156932,234.45C166.13191322137317,223.40263337893296,214.3352773009808,171.83625,238.43695934078465,172.5C262.5386413805885,173.16375,310.74200546019614,233.1225,334.8436875,239.76C358.94536953980383,246.39749999999998,407.1487336194115,240.62441860465117,431.2504156592153,225.6C455.4181297046076,210.53441860465117,503.7535577953923,117.18447332421341,527.9212718407846,119.4C552.0229538805884,121.60947332421341,600.2263179601962,212.32500000000002,624.328,243.3L624.328,261L45.359375,261Z" fill-opacity="0.7" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 0.7;"></path><path fill="none" stroke="#fcc24c" d="M45.359375,261C69.52708904539233,254.3625,117.86251713617699,245.52763337893296,142.03023118156932,234.45C166.13191322137317,223.40263337893296,214.3352773009808,171.83625,238.43695934078465,172.5C262.5386413805885,173.16375,310.74200546019614,233.1225,334.8436875,239.76C358.94536953980383,246.39749999999998,407.1487336194115,240.62441860465117,431.2504156592153,225.6C455.4181297046076,210.53441860465117,503.7535577953923,117.18447332421341,527.9212718407846,119.4C552.0229538805884,121.60947332421341,600.2263179601962,212.32500000000002,624.328,243.3" stroke-width="0" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><circle cx="45.359375" cy="261" r="0" fill="#fcc24c" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="142.03023118156932" cy="234.45" r="0" fill="#fcc24c" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="238.43695934078465" cy="172.5" r="0" fill="#fcc24c" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="334.8436875" cy="239.76" r="0" fill="#fcc24c" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="431.2504156592153" cy="225.6" r="0" fill="#fcc24c" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="527.9212718407846" cy="119.4" r="0" fill="#fcc24c" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="624.328" cy="243.3" r="0" fill="#fcc24c" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><path fill="#8db1ec" stroke="none" d="M45.359375,261C69.52708904539233,258.7875,117.86251713617699,261,142.03023118156932,252.15C166.13191322137317,237.78842339261286,214.3352773009808,146.39249999999998,238.43695934078465,145.95C262.5386413805885,145.5075,310.74200546019614,260.77875,334.8436875,248.61C358.94536953980383,236.44125000000003,407.1487336194115,55.89126196990427,431.2504156592153,48.60000000000002C455.4181297046076,41.288761969904265,503.7535577953923,165.82920656634747,527.9212718407846,190.2C552.0229538805884,214.50420656634748,600.2263179601962,230.025,624.328,243.3L624.328,261L45.359375,261Z" fill-opacity="0.7" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 0.7;"></path><path fill="none" stroke="#508aeb" d="M45.359375,261C69.52708904539233,258.7875,117.86251713617699,261,142.03023118156932,252.15C166.13191322137317,237.78842339261286,214.3352773009808,146.39249999999998,238.43695934078465,145.95C262.5386413805885,145.5075,310.74200546019614,260.77875,334.8436875,248.61C358.94536953980383,236.44125000000003,407.1487336194115,55.89126196990427,431.2504156592153,48.60000000000002C455.4181297046076,41.288761969904265,503.7535577953923,165.82920656634747,527.9212718407846,190.2C552.0229538805884,214.50420656634748,600.2263179601962,230.025,624.328,243.3" stroke-width="0" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><circle cx="45.359375" cy="261" r="0" fill="#508aeb" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="142.03023118156932" cy="252.15" r="0" fill="#508aeb" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="238.43695934078465" cy="145.95" r="0" fill="#508aeb" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="334.8436875" cy="248.61" r="0" fill="#508aeb" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="431.2504156592153" cy="48.60000000000002" r="0" fill="#508aeb" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="527.9212718407846" cy="190.2" r="0" fill="#508aeb" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="624.328" cy="243.3" r="0" fill="#508aeb" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle></svg><div class="morris-hover morris-default-style" style="left: 2.79688px; top: 137px; display: none;"><div class="morris-hover-row-label">2012</div><div class="morris-hover-point" style="color: #ff5560">
                    Series A:
                    0
                    </div><div class="morris-hover-point" style="color: #fcc24c">
                    Series B:
                    0
                    </div><div class="morris-hover-point" style="color: #508aeb">
                    Series C:
                    0
                    </div></div></div>
                </div>
            </div>
        </div>
        <div class="col-xl-4">
            <div class="card m-b-30">
                <div class="card-body">
                    <h4 class="mt-0 header-title">Revenue</h4>

                    <ul class="list-inline widget-chart m-t-20 text-center">
                    <li>
                    <h4 class=""><b>5248</b></h4>
                    <p class="text-muted m-b-0">Marketplace</p>
                    </li>
                    <li>
                    <h4 class=""><b>321</b></h4>
                    <p class="text-muted m-b-0">Last week</p>
                    </li>
                    <li>
                    <h4 class=""><b>964</b></h4>
                    <p class="text-muted m-b-0">Last Month</p>
                    </li>
                    </ul>
                    <div id="morris-bar-example" style="height: 300px; position: relative; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"><svg height="300" version="1.1" width="289.656" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="overflow: hidden; position: relative; left: -0.328125px;"><desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.1.2</desc><defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs><text x="32.859375" y="261" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">0</tspan></text><path fill="none" stroke="#eef0f2" d="M45.359375,261H264.656" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="32.859375" y="202" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">25</tspan></text><path fill="none" stroke="#eef0f2" d="M45.359375,202H264.656" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="32.859375" y="143" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">50</tspan></text><path fill="none" stroke="#eef0f2" d="M45.359375,143H264.656" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="32.859375" y="84" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">75</tspan></text><path fill="none" stroke="#eef0f2" d="M45.359375,84H264.656" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="32.859375" y="25" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">100</tspan></text><path fill="none" stroke="#eef0f2" d="M45.359375,25H264.656" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="242.7263375" y="273.5" text-anchor="middle" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2018</tspan></text><text x="155.0076875" y="273.5" text-anchor="middle" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2016</tspan></text><text x="67.2890375" y="273.5" text-anchor="middle" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal" transform="matrix(1,0,0,1,0,7)"><tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2014</tspan></text><rect x="58.5171725" y="84" width="7.271865" height="177" rx="0" ry="0" fill="#508aeb" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="68.7890375" y="107.6" width="7.271865" height="153.4" rx="0" ry="0" fill="#fcc24c" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="102.3764975" y="143" width="7.271865" height="118" rx="0" ry="0" fill="#508aeb" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="112.6483625" y="166.60000000000002" width="7.271865" height="94.39999999999998" rx="0" ry="0" fill="#fcc24c" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="146.23582249999998" y="84" width="7.271865" height="177" rx="0" ry="0" fill="#508aeb" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="156.50768749999997" y="107.6" width="7.271865" height="153.4" rx="0" ry="0" fill="#fcc24c" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="190.09514749999997" y="25" width="7.271865" height="236" rx="0" ry="0" fill="#508aeb" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="200.36701249999996" y="48.60000000000002" width="7.271865" height="212.39999999999998" rx="0" ry="0" fill="#fcc24c" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="233.95447249999998" y="48.60000000000002" width="7.271865" height="212.39999999999998" rx="0" ry="0" fill="#508aeb" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="244.22633749999997" y="84" width="7.271865" height="177" rx="0" ry="0" fill="#fcc24c" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect></svg><div class="morris-hover morris-default-style" style="left: 21.6718px; top: 104px; display: none;"><div class="morris-hover-row-label">2014</div><div class="morris-hover-point" style="color: #508aeb">
                    Series A:
                    75
                    </div><div class="morris-hover-point" style="color: #fcc24c">
                    Series B:
                    65
                    </div></div></div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xl-12">
            <div class="card m-b-30">
                <div class="card-body">
                    <h4 class="mt-0 header-title mb-4">Recent Customer</h4>
                    <div class="table-responsive">
                        <table class="table table-hover mb-0">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Created At</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($recent_customer as $item)
                                    <tr>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->email }}</td>
                                        <td>{{ $item->phone }}</td>
                                        <td>{{ $item->created_at->format('F j, Y') }}</td>
                                    </tr>
                                @empty
                                    <tr><td colspan="9">No recent customer found</td> </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('page_scripts')

@endpush
