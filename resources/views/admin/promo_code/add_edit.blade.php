<div class="modal-header bg-success">
<h5 class="modal-title" id="workerLabel">{{$data->id ? 'Edit' : 'Add'}} Record</h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body">
    @php
        $today = date('Y-m-d');
    @endphp
    {{ Form::model($data,array('route' => array('admin.promo-codes.store'),'method' => 'post','id'=>"js_modal_form",'enctype'=>"multipart/form-data",'novalidate'=>true)) }}
        {{Form::hidden('id')}}
        <div class="row">
            <div class="form-group col-md-12">
                {{Form::label('title','Title')}}

                {{Form::text('title',null,
                            array(  'placeholder'   => 'Title',
                                    'class'         => 'form-control',
                                    'id'            => 'title')
                                    )}}
                <span class="text-danger error-msg-input"></span>
            </div>
            <div class="form-group col-md-6">
                {{Form::label('code','Code')}}

                {{Form::text('code',null,
                            array(  'placeholder'   => 'Code',
                                    'class'         => 'form-control js_valid required',
                                    'id'            => 'code')
                                    )}}
                <span class="text-danger error-msg-input"></span>
            </div>
            <div class="form-group col-md-6">
                {{Form::label('discount_percent','Descount (%)')}}

                {{Form::text('discount_percent',null,
                            array(  'placeholder'   => 'Descount (%)',
                                    'class'         => 'form-control js_valid number',
                                    'id'            => 'discount_percent')
                                    )}}
                <span class="text-danger error-msg-input"></span>
            </div>
            <div class="form-group col-md-6">
                {{Form::label('start_date','Start Date')}}

                {{Form::date('start_date',null,
                            array(  'placeholder'   => 'Start Date',
                                    'class'         => 'form-control js_valid required',
                                    'min'           => $today,
                                    'id'            => 'start_date')
                                    )}}
                <span class="text-danger error-msg-input"></span>
            </div>
            <div class="form-group col-md-6">
                {{Form::label('end_date','End Date')}}

                {{Form::date('end_date',null,
                            array(  'placeholder'   => 'End Date',
                                    'class'         => 'form-control js_valid required',
                                    'min'           => $today,
                                    'id'            => 'end_date')
                                    )}}
                <span class="text-danger error-msg-input"></span>
            </div>
            <div class="form-group col-md-6">
                {{Form::label('is_banner','Display as a banner on app')}} <br>
                <label class="switch">
                    <input type="checkbox" name="is_banner" class="js_banner form-control" {{ $data->is_banner ? 'checked' : '' }}>
                    <span class="slider"></span>
                </label>
                <span class="text-danger error-msg-input"></span>
            </div>
            <div class="form-group col-md-3">
                {{Form::label('image','Banner for app screen')}}

                {{Form::file('image',
                        array(  'placeholder'   => 'Image',
                                'class'         => 'form-control',
                                'id'            => 'image')
                                )}}
                <span class="text-danger error-msg-input"></span>
            </div>
            <div class="form-group col-md-3">
                @if(isset($data->image) && !empty($data->image))
                <img src="{{ $data->image }}" height="80px" width="80px" />
                @endif
            </div>
            <div class="form-group col-md-12">
                {{Form::label('description','Description')}}

                {{Form::textarea('description',null,
                        array(  'placeholder'   => 'Description',
                                'class'         => 'form-control',
                                'id'            => 'description',
                                'rows'           =>  3)
                                )}}
                <span class="text-danger error-msg-input"></span>
            </div>
        </div>
        <div class="box-footer">
            <button type="reset" data-dismiss="modal" class="btn btn-default">Cancel</button>
            {{Form::button('Save',array('class'=>'btn btn-success pull-right','type'=>'submit'))}}
        </div>
</div>
