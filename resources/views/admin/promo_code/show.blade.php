<div class="modal-header bg-success">
    <h5 class="modal-title" id="workerLabel">Promo Code Details</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <table class="table mb-0">
        <tr>
            <th>Title</th>
            <td>{{ $data->title }}</td>
            <th>Promo Code</th>
            <td>{{ $data->code }}</td>
        </tr>
        <tr>
            <th>Start Date</th>
            <td>{{ $data->start_date }}</td>
            <th>End Date</th>
            <td>{{ $data->end_date }}</td>
        </tr>
        <tr>
            <th>Status</th>
            <td>
                @if ($data->is_active)
                    <span class="badge badge-success">Active</span>
                @else
                    <span class="badge badge-danger">In Active</span>
                @endif
            </td>
            <th>Description</th>
            <td>{{ $data->description }}</td>
        </tr>
    </table>
</div>
