<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('restaurant_id')->nullable()->constrained('restaurants')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('table_id')->constrained('tables')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('promo_code_id')->nullable()->constrained('promo_codes')->onUpdate('cascade')->onDelete('cascade');
            $table->string('promo_code')->nullable();
            $table->string('payment_method')->nullable()->comment('Paylands, 2 = Apple Pay');
            $table->string('card_number')->nullable()->comment('last four digit of card');
            $table->integer('promo_code_discount_percent')->nullable();
            $table->double('discounted_price', 8, 2)->nullable();
            $table->double('actual_price', 8, 2)->nullable();
            $table->double('total_amount', 8, 2)->nullable();
            $table->string('payment_id')->nullable();
            $table->tinyInteger('payment_type')->default(1)->comment('1 = Cash, 2 = Card');
            $table->boolean('payment_status')->default(0)->comment('0 = Un-Paid, 1 = Paid');
            $table->longText('payment_details')->nullable()->comment('It will json array of payment object');
            $table->tinyInteger('order_processing')->default(1)->comment('1 = Processing, 2 = Complete');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
