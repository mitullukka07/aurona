<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('phone')->unique()->nullable();
            $table->string('email')->unique()->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->string('facebook_id')->nullable();
            $table->string('google_id')->nullable();
            $table->string('apple_id')->nullable();
            $table->tinyInteger('social_type')->nullable()->comment('1=Facebook, 2=Goolge','3-Apple');
            $table->boolean('is_verify')->default(0)->comment('1 = Verify, 0 = No');
            $table->boolean('is_active')->default(0)->comment('1 = Active, 0 = Inactive');
            $table->tinyInteger('device_type')->nullable()->comment('1 = Android, 2 = Ios');
            $table->string('device_token')->nullable();
            $table->string('current_version')->nullable();
            $table->string('default_language')->nullable();
            $table->string('otp')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
