<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_details', function (Blueprint $table) {
            $table->id();
            $table->foreignId('order_id')->constrained('orders')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('item_id')->constrained('items')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('quantity')->nullable();
            $table->double('item_price', 8, 2)->default(0)->nullable();
            $table->double('addon_price', 8, 2)->default(0)->nullable();
            $table->double('total_price', 8, 2)->nullable();
            $table->longText('addon_details')->nullable()->comment('This will json array of addon details, id,quantity,price,total_price');
            $table->tinyInteger('item_status')->default(1)->comment('1 = Being Prepared, 2 = Ready to take');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_details');
    }
}
