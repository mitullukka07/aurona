<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middleware' => ['decrypt_req']], function ($router) {
    Route::post('registration',                     'UserController@registration');
    Route::post('login',                            'UserController@login');


    Route::post('forgot-password' ,             'UserController@forgotPassword');
    Route::post('resend-otp',                   'UserController@resendOtp');
    Route::post('verify-otp',                   'UserController@verifyOtp');
    Route::post('reset-password' ,              'UserController@resetPassword');

    Route::post('restaurants' ,                 'RestaurantController@restaurants');
    Route::post('restaurant-detail' ,           'RestaurantController@restaurantDetail');

    Route::post('cms' ,                         'CommonController@cms');
    Route::post('category' ,                    'CommonController@category');
    Route::post('banner' ,                      'CommonController@banner');
    Route::post('item' ,                        'CommonController@item');
    Route::post('addon' ,                       'CommonController@addon');
});

Route::middleware(['auth:api','decrypt_req'])->group(function () {
    Route::post('logout',                   'UserController@logout');
    Route::post('change-password',          'UserController@changePassword');

    Route::post('profile',                  'UserController@getProfile');
    Route::post('update-profile',           'UserController@updateUserProfile');
    Route::post('store-card',               'UserController@storeCard');
    Route::post('update-card',              'UserController@updateCard');
    Route::post('delete-card',              'UserController@deleteCard');
    Route::post('card-list',                'UserController@cardList');

    Route::post('notification',             'CommonController@notification');
    Route::post('rating',                   'CommonController@rating');
    Route::post('check-promo-code',         'CommonController@checkPromoCode');
    Route::post('resend-email',             'CommonController@resendEmail');
    Route::post('promo-codes',              'CommonController@promoCodes');
    Route::post('add-promo-codes',          'CommonController@addPromoCode');

    Route::post('store-order',              'OrderController@storeOrder');
    Route::post('my-order',                 'OrderController@myOrders');
    Route::post('order-detail',             'OrderController@orderDetail');
    // Route::middleware('is_user_verify')->group(function () {

        //     Route::get('get-profile',                   'UserController@getProfile');
        //     Route::post('update-profile',               'UserController@updateUserProfile');

        // });
});
