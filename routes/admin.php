<?php

Route::get('Admin2020',  function(){
    return redirect()->route('admin.login');
});

Route::group(['namespace' => 'Auth'], function(){
    # Login Routes
    Route::get('login',     'LoginController@showLoginForm')->name('login');
    Route::post('login',    'LoginController@login');
    Route::post('logout',   'LoginController@logout')->name('logout');

});

Route::group(['middleware' => 'auth:admin'],function (){

    # dashboard
    Route::get('/dashboard',                'DashboardController@index')->name('dashboard');

    # admin update password
    Route::get('change-password',           'DashboardController@changePassword')->name('change-password');
    Route::patch('update-password',         'DashboardController@updatePassword')->name('update-password');



    # FAQ
    Route::patch('faq/change-status',        'FaqController@changeStatus')->name('faq.change-status');
    Route::post('faq/delete',               'FaqController@delete')->name('faq.delete');
    Route::post('faq/bulk-delete',          'FaqController@bulkDelete')->name('faq.bulk-delete');
    Route::resource('faq',                  'FaqController');

    # Customer
    Route::resources([
        'category'      => CategoryController::class,
        'customers'     => UserController::class,
        'restaurants'   => RestaurantController::class,
        'cms'           => CmsController::class,
        'notification'  => NotificationController::class,
        'settings'      => SettingController::class,
        'promo-codes'   => PromoCodeController::class,
        'addon'         => AddonController::class,
        'item'          => ItemController::class,
        'table'         => TableController::class,
        'order'         => OrderController::class,
    ]);

    # category
    Route::patch('category-change-status',       'CategoryController@changeStatus')->name('category.change_status');

    # Customer
    Route::patch('customer-change-status',       'UserController@changeStatus')->name('customers.change_status');

    # Restaurant
    Route::patch('restaurant-change-status',     'RestaurantController@changeStatus')->name('restaurants.change_status');

    #Cms
    Route::patch('cms-change-status',       'CmsController@changeStatus')->name('cms.change_status');

    #PromoCode
    Route::patch('promo-codes-change-status',       'PromoCodeController@changeStatus')->name('promo-codes.change_status');
    Route::get('promo-codes-users',                 'PromoCodeController@promoCodeUser')->name('promo-codes.users');
    Route::patch('promo-codes-users-delete',        'PromoCodeController@destroyUserPromoCode')->name('promo-codes.users_delete');

    #Addon
    Route::patch('addon-change-status',       'AddonController@changeStatus')->name('addon.change_status');

    #Item
    Route::patch('item-change-status',       'ItemController@changeStatus')->name('item.change_status');
    Route::get('category-list',              'ItemController@categoryList')->name('category.list');

    #Table
    Route::patch('table-change-status',       'TableController@changeStatus')->name('table.change_status');

    #Order
    Route::patch('order-complete',       'OrderController@orderCompleted')->name('order.order_complete');
    Route::patch('order-ready-to-take',       'OrderController@orderReadyToTake')->name('order.ready_to_take');

});

?>
