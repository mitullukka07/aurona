<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('admin-panel',  function(){
    return redirect()->route('admin.login');
});

// Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/contact-us', 'HomeController@contact')->name('contact');
Route::get('/about-us', 'HomeController@about')->name('about');
Route::get('/product-details', 'HomeController@product_details')->name('product_details');
Route::get('/services', 'HomeController@services')->name('services');

// Route::get('/custom-link','Api\CommonController@customLink');
// // ios applink
// Route::get('apple-app-site-association','Api\CommonController@appleAppsiteAssociation');

