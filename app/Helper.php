<?php

use App\Models\Category;
use App\Models\User;
use App\Models\Setting;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

if ( ! function_exists('uploadFile'))
{
    function uploadFile($file, $dir)
    {
        if ($file) {

            $destinationPath =  storage_path('app/public'). DIRECTORY_SEPARATOR . $dir . DIRECTORY_SEPARATOR;

            $media_image = $file->hashName();

            $file->move($destinationPath, $media_image);

            return $media_image;
        }
    }
}

function getUserData($id)
{
    $user = User::find($id);
    if($user)
        return User::select(['id', 'name', 'email','phone'])->find($user->id);
    return null;
}

function sendPushNotification($data, $to, $options) {
    // Insert your Secret API Key here
    $apiKey = 'c38757f42bbf39036ae2093ed5c54ad379289779376a8eb0a3afdaa9732be477';

    // Default post data to provided options or empty array
    $post = $options ?: array();

    // Set notification payload and recipients
    $post['to'] = $to;
    $post['data'] = $data;

    // Set Content-Type header since we're sending JSON
    $headers = array(
        'Content-Type: application/json'
    );

    // Initialize curl handle
    $ch = curl_init();

    // Set URL to Pushy endpoint
    curl_setopt($ch, CURLOPT_URL, 'https://api.pushy.me/push?api_key=' . $apiKey);

    // Set request method to POST
    curl_setopt($ch, CURLOPT_POST, true);

    // Set our custom headers
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    // Get the response back as string instead of printing it
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    // Set post data as JSON
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post, JSON_UNESCAPED_UNICODE));

    // Actually send the push
    $result = curl_exec($ch);

    // Display errors
    if (curl_errno($ch)) {
        echo curl_error($ch);
    }

    // Close curl handle
    curl_close($ch);

    // Attempt to parse JSON response
    $response = @json_decode($result);

    // Throw if JSON error returned
    if (isset($response) && isset($response->error)) {
        throw new Exception('Pushy API returned an error: ' . $response->error);
    }
}

function adminSetting() {
    $data = Setting::where('key','admin_setting')->first();
    if ($data) {
        $data = $data->value;
    }
    return $data;
}

function cmsPages() {
    return array(
        'term_of_service'       => 'Term of service',
        'privacy_policy'        => 'Privacy Policy',
        'about_us'              => 'About Us'
    );
}

function getRestaurantCategory($category_ids = []){
    if (!empty($category_ids)) {
        return Category::whereIn('id',$category_ids)->where('is_active',1)->get(['id','name']);
    }
    return [];
}

function storeQrCodeImages($restaurant_id = null, $table_id = null) {
    if ($restaurant_id && $table_id) {
        $qrNameCode         = time() . date('Y-m-d') . '.png';
        $StorageLocation    = '/storage/qr_code/';
        $path               = public_path() . $StorageLocation;
        $fileStore          = $path . $qrNameCode;
        $url                = config('app.url').'?restaurant='.$restaurant_id.'&table='.$table_id;

        \File::makeDirectory($path, 0777, true, true);

        \QrCode::size(250)
        ->format('png')
        ->generate($url, $fileStore);
        return $qrNameCode;
    }
    abort(404);
}



?>
