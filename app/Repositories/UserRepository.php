<?php
namespace App\Repositories;

use App\Contracts\UserContract;
use App\Models\User;

class UserRepository extends BaseRepository implements UserContract
{
    # Note : extends BaseRepository for basic functionality
    #       : if you want to change in existing function then only override existing function here else no need to define any function here.

    public function __construct(User $model)
    {
        $this->model = $model;
    }

}

?>
