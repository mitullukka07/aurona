<?php
namespace App\Repositories;

use App\Contracts\ItemContract;
use App\Models\Item;

class ItemRepository extends BaseRepository implements ItemContract
{
    # Note : extends BaseRepository for basic functionality
    #       : if you want to change in existing function then only override existing function here else no need to define any function here.

    public function __construct(Item $model)
    {
        $this->model = $model;
    }

}

?>
