<?php
namespace App\Repositories;

use App\Contracts\RestaurantContract;
use App\Models\Restaurant;

class RestaurantRepository extends BaseRepository implements RestaurantContract
{
    # Note : extends BaseRepository for basic functionality
    #       : if you want to change in existing function then only override existing function here else no need to define any function here.

    public function __construct(Restaurant $model)
    {
        $this->model = $model;
    }

}

?>
