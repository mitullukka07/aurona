<?php
namespace App\Repositories;

use App\Contracts\NotificationContract;
use App\Models\Notification;

class NotificationRepository extends BaseRepository implements NotificationContract
{
    # Note : extends BaseRepository for basic functionality
    #       : if you want to change in existing function then only override existing function here else no need to define any function here.

    public function __construct(Notification $model)
    {
        $this->model = $model;
    }

}

?>
