<?php
namespace App\Repositories;

use App\Contracts\PromoCodeContract;
use App\Models\PromoCode;

class PromoCodeRepository extends BaseRepository implements PromoCodeContract
{
    # Note : extends BaseRepository for basic functionality
    #       : if you want to change in existing function then only override existing function here else no need to define any function here.

    public function __construct(PromoCode $model)
    {
        $this->model = $model;
    }

}

?>
