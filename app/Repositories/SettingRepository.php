<?php
namespace App\Repositories;

use App\Contracts\SettingContract;
use App\Models\Setting;

class SettingRepository extends BaseRepository implements SettingContract
{
    # Note : extends BaseRepository for basic functionality
    #       : if you want to change in existing function then only override existing function here else no need to define any function here.

    public function __construct(Setting $model)
    {
        $this->model = $model;
    }

    public function create(array $data)
    {
        return $this->model->create($data);
    }

    public function update(array $data, $id)
    {
        $record = $this->findData($id);
        $data   = array('value' => $data);
        return $record->update($data);
    }

}

?>
