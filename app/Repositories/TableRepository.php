<?php
namespace App\Repositories;

use App\Contracts\TableContract;
use App\Models\Table;

class TableRepository extends BaseRepository implements TableContract
{
    # Note : extends BaseRepository for basic functionality
    #       : if you want to change in existing function then only override existing function here else no need to define any function here.

    public function __construct(Table $model)
    {
        $this->model = $model;
    }

}

?>
