<?php
namespace App\Repositories;

use App\Models\Category;
use App\Contracts\CategoryContract;
use Illuminate\Database\Eloquent\Model;


class CategoryRepository extends BaseRepository implements CategoryContract
{
    # Note : extends BaseRepository for basic functionality
    #       : if you want to change in existing function then only override existing function here else no need to define any function here.
    public function __construct(Category $model)
    {
        $this->model = $model;
    }


}

?>
