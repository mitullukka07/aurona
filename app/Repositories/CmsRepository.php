<?php
namespace App\Repositories;

use App\Contracts\CmsContract;
use App\Models\Cms;

class CmsRepository extends BaseRepository implements CmsContract
{
    # Note : extends BaseRepository for basic functionality
    #       : if you want to change in existing function then only override existing function here else no need to define any function here.

    public function __construct(Cms $model)
    {
        $this->model = $model;
    }

}

?>
