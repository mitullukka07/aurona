<?php
namespace App\Repositories;

use App\Contracts\OrderContract;
use App\Models\Order;

class OrderRepository extends BaseRepository implements OrderContract
{
    # Note : extends BaseRepository for basic functionality
    #       : if you want to change in existing function then only override existing function here else no need to define any function here.

    public function __construct(Order $model)
    {
        $this->model = $model;
    }

}

?>
