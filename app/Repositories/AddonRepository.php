<?php
namespace App\Repositories;

use App\Contracts\AddonContract;
use App\Models\Addon;

class AddonRepository extends BaseRepository implements AddonContract
{
    # Note : extends BaseRepository for basic functionality
    #       : if you want to change in existing function then only override existing function here else no need to define any function here.

    public function __construct(Addon $model)
    {
        $this->model = $model;
    }

}

?>
