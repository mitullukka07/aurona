<?php

namespace App\Providers;

use App\Contracts\BaseContract;
use App\Contracts\UserContract;
use App\Contracts\CmsContract;
use App\Contracts\RestaurantContract;
use App\Contracts\SettingContract;
use App\Contracts\CategoryContract;
use App\Contracts\NotificationContract;
use App\Contracts\PromoCodeContract;
use App\Contracts\AddonContract;
use App\Contracts\ItemContract;
use App\Contracts\TableContract;
use App\Contracts\OrderContract;

use App\Repositories\BaseRepository;
use App\Repositories\UserRepository;
use App\Repositories\CmsRepository;
use App\Repositories\RestaurantRepository;
use App\Repositories\SettingRepository;
use App\Repositories\CategoryRepository;
use App\Repositories\NotificationRepository;
use App\Repositories\PromoCodeRepository;
use App\Repositories\AddonRepository;
use App\Repositories\ItemRepository;
use App\Repositories\TableRepository;
use App\Repositories\OrderRepository;

use Illuminate\Support\ServiceProvider;


class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(BaseContract::class, BaseRepository::class);
        $this->app->bind(UserContract::class, UserRepository::class);
        $this->app->bind(CmsContract::class, CmsRepository::class);
        $this->app->bind(CategoryContract::class, CategoryRepository::class);
        $this->app->bind(RestaurantContract::class, RestaurantRepository::class);
        $this->app->bind(SettingContract::class, SettingRepository::class);
        $this->app->bind(NotificationContract::class, NotificationRepository::class);
        $this->app->bind(PromoCodeContract::class, PromoCodeRepository::class);
        $this->app->bind(AddonContract::class, AddonRepository::class);
        $this->app->bind(ItemContract::class, ItemRepository::class);
        $this->app->bind(TableContract::class, TableRepository::class);
        $this->app->bind(OrderContract::class, OrderRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
