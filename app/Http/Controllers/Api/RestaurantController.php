<?php

namespace App\Http\Controllers\Api;

use App\Models\Category;
use App\Models\Restaurant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class RestaurantController extends BaseController
{
    public function restaurants(Request $request)
    {
        $restaurants        = new Restaurant();
        if (!empty($request->name)) {
            $restaurants    = $restaurants->where('name','LIKE','%'.$request->name.'%');
        }

        if(!empty(request('last_id'))){
            $restaurants    = $restaurants->where('id', '<', request('last_id'));
        }

        $restaurants        = $restaurants->limit(10)
                            ->orderBy('id','desc')
                            ->get(['id','name','email','mobile','address','description','restaurant_time','lat','long','image']);
        return $this->sendResponse($restaurants);
    }

    public function restaurantDetail(Request $request)
    {
        $request->validate(['id' => 'required|exists:restaurants,id,deleted_at,NULL']);
        $restaurant     = Restaurant::find($request->id,['id','name','email','mobile','address','description','restaurant_time','lat','long','image','category_ids']);
        $category_ids   = $restaurant->category_ids;
        $item_name      = $request->item_name;
        unset($restaurant->category_ids);
        $category       = Category::whereHas('items', function($q) use ($item_name){
                                if (!empty($item_name)) {
                                   $q->where('name','LIKE','%'.$item_name.'%');
                                }
                            })
                            ->with(['items' => function($q) use ($item_name){
                                if (!empty($item_name)) {
                                    $q =  $q->where('items.name','LIKE','%'.$item_name.'%');
                                }
                                $q->select(['id','category_id','name','images','price','description']);
                            }])
                            ->whereIn('id',$category_ids)
                            ->where('is_active',1)
                            ->get(['id','name']);
        $restaurant->category = $category;
        return $this->sendResponse($restaurant);
    }


}
