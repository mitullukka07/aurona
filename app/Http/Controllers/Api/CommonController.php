<?php

namespace App\Http\Controllers\Api;

use Mobile_Detect;
use App\Models\Cms;
use App\Models\Item;
use App\Mail\Invoice;
use App\Models\Addon;
use App\Models\Order;
use App\Models\Rating;
use App\Models\Category;
use App\Models\PromoCode;
use App\Models\Restaurant;
use App\Models\Notification;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class CommonController extends BaseController
{
    public function cms(Request $request)
    {
        $request->validate(['page' => 'required|in:term_of_service,privacy_policy,about_us']);
        $restaurants = Cms::where('page', $request->page)->first(['id', 'title', 'page', 'content']);
        return $this->sendResponse($restaurants);
    }

    public function notification(Request $request)
    {
        $user   = $request->user();
        $lists  = Notification::where('user_id', $user->id)->orderBy('id', 'desc')->get(['id', 'title', 'description', 'created_at']);
        return $this->sendResponse($lists);
    }

    public function rating(Request $request)
    {
        $request->validate(['restaurant_id' => 'required|exists:restaurants,id', 'rating' => 'required|in:0,1,2']);
        $user        = $request->user();
        $rating      = Rating::where('restaurant_id', $request->restaurant_id)->where('user_id', $user->id)->first();
        if (empty($rating)) {
            $inputs             = $request->all();
            $inputs['user_id']  = $user->id;
            Rating::Create($inputs);
            return $this->sendResponse(["message" => trans("You have succesfully rated")]);
        }
        return $this->sendValidationError("message", trans("You have already given rating to this restaurant"));
    }

    public function banner(Request $request)
    {
        $today       = date('Y-m-d');
        $conditions  = ['is_active' => 1, 'is_banner' => 1];
        $restaurants = PromoCode::where($conditions)
            ->where('start_date', '<=', $today)
            ->where('end_date', '>=', $today)
            ->get(['id', 'title', 'code', 'description', 'image']);
        return $this->sendResponse($restaurants);
    }

    public function item(Request $request)
    {
        $request->validate([
            'restaurant_id' => 'required|exists:restaurants,id',
            'category_id'   => 'required|exists:categories,id'
        ]);
        $conditions  = [
            'is_active'     => 1,
            'restaurant_id' => $request->restaurant_id,
            'category_id'   => $request->category_id
        ];
        $items = Item::where($conditions)->get(['id', 'name', 'images', 'price', 'description']);
        return $this->sendResponse($items);
    }

    public function addon(Request $request)
    {
        $request->validate(['category_id'   => 'required|exists:categories,id']);
        $conditions     = [
            'is_active'     => 1,
            'id'            => $request->category_id
        ];
        $category       = Category::where($conditions)->first(['id', 'addon_ids']);
        $addons         = [];
        if (isset($category->addon_ids) && !empty($category->addon_ids)) {
            $addons         = Addon::where('is_active', 1)->whereIn('id', $category->addon_ids)->get(['id', 'name', 'price', 'description']);
        }
        return $this->sendResponse($addons);
    }

    public function checkPromoCode(Request $request)
    {
        $request->validate(['code'   => 'required|exists:promo_codes,code']);
        $today              = date('Y-m-d');
        $promo_code         = $request->code;
        $is_valid_promo     = PromoCode::where('code',$promo_code)
                                ->where('is_active',1)
                                ->where('start_date','<=',$today)
                                ->where('end_date','>=',$today)
                                ->first(['id','title','code','end_date','discount_percent','description']);
        if ($is_valid_promo) {
            return $this->sendResponse($is_valid_promo);
        }else{
            return $this->sendValidationError("message",trans("Provided promo code is invalid or expired"));
        }

    }

    public function resendEmail(Request $request)
    {
        $request->validate(['order_id'   => 'required|exists:orders,id']);
        $user        = $request->user();
        $order       = Order::with(['restaurant:id,name,image',
                                'order_details:id,item_id,order_id,item_price,addon_price,total_price,addon_details,item_status',
                                'order_details.item:id,name',
                                'table:id,name'])
                                ->where('id', $request->order_id)
                                ->where('user_id', $user->id)
                                ->first();
        if ($order) {
            Mail::to($user->email)->send(new Invoice($order,$user));
            return $this->sendResponse(["message" => trans("Invoice successfully sent on your email")]);
        }
        return $this->sendValidationError("message", trans("You have provided invalid order"));
    }

    public function promoCodes(Request $request)
    {
        $user            = $request->user();
        $promo_codes     = $user->promoCodes()->get(['id','title','code','description','end_date','discount_percent']);

        return $this->sendResponse($promo_codes);

    }

    public function addPromoCode(Request $request)
    {
        $request->validate(['code'   => 'required|exists:promo_codes,code']);
        $today              = date('Y-m-d');
        $promo_code         = $request->code;
        $user               = $request->user();
        $is_valid_promo     = PromoCode::where('code',$promo_code)
                                ->where('is_active',1)
                                ->where('start_date','<=',$today)
                                ->where('end_date','>=',$today)
                                ->first();
        if ($is_valid_promo) {
            $check = $is_valid_promo->users()->where('user_id',$user->id)->exists();
            $msg   = "Promocode already added";
            if (!$check) {
                $is_valid_promo->users()->attach($user->id,['created_at' => date('Y-m-d H:i:s')]);
                $msg = "Promocode added successfully";
            }
            return $this->sendResponse(["message" => trans($msg)]);
        }else{
            return $this->sendValidationError("message",trans("Provided promo code is invalid or expired"));
        }
    }

    public function customLink(Request $request)
    {
        $detect                 = new Mobile_Detect;
        $data['restaurant']     = $request->restaurant;
        $data['table']          = $request->table;
        $data['isAndroidOS']    = $detect->isAndroidOS();
        $data['isiOS']          = $detect->isiOS();
        return view('common.customlink', compact('data'));
    }


    public function appleAppsiteAssociation()
    {
        $details = array(
            'appID' => '8C3X937F55.com.app.mimapp',
            'paths' => ["NOT /_/*", "/*"],
        );
        // $details = array(
        //     $dataSet,
        // );
        $applinks = array(
            'apps' => [],
            'details' => $details
        );

        $data = array(
            'applinks' => $applinks
        );
        return response()->json($data, 200);
    }
}
