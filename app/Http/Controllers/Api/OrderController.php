<?php

namespace App\Http\Controllers\Api;

use App\Models\Addon;
use App\Models\Category;
use App\Models\Item;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\PromoCode;
use App\Models\Restaurant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class OrderController extends BaseController
{
    public function storeOrder(Request $request)
    {
        $request->validate(['restaurant_id' => 'required|exists:restaurants,id,deleted_at,NULL',
        'table_id'      => 'required|exists:tables,id,deleted_at,NULL',
        'payment_type'  => 'required|in:1,2',
        'item'          => 'required',

        ]);
        $user               = $request->user();
        $items              = json_decode($request->item);
        $promo_code         = $request->promo_code;
        $table_id           = $request->table_id;
        $restaurant_id      = $request->restaurant_id;
        $payment_type       = $request->payment_type;
        $user_id            = $user->id;
        $payment_details    = $request->payment_response;
        $actual_price       = $discounted_price   = $total_amount       = 0;
        $today              = date('Y-m-d');
        $is_valid_promo     = PromoCode::where('code',$promo_code)
                                ->where('is_active',1)
                                ->where('start_date','<=',$today)
                                ->where('end_date','>=',$today)
                                ->first();

        if ($is_valid_promo || empty($promo_code)) {
            DB::beginTransaction();
            try {
                $order = new Order();
                $order->restaurant_id   = $restaurant_id;
                $order->table_id        = $table_id;
                $order->user_id         = $user_id;
                $order->payment_method  = $request->payment_method;
                $order->card_number     = $request->card_number;
                $order->save();

                $order_detail_data = array();

                foreach ($items as $key => $value) {
                    $item_data = Item::find($value->item_id);

                    $item_id        = $item_data->id;
                    $item_qty       = $value->item_qty;
                    $item_price     = $item_data->price;
                    $addon_price    = 0;
                    $addon_details  = [];
                    $addon_ids      = [];

                    if (isset($value->addon_details) && !empty($value->addon_details)) {
                        foreach ($value->addon_details as $key => $addon) {
                            $addon_ids[] = $addon->addon_id;
                            $addon_qtys[$addon->addon_id] = $addon->addon_qty;
                        }
                        $addons_data = Addon::whereIn('id',$addon_ids)->get(['id','name','price']);

                        foreach ($addons_data as  $addon) {
                            $addon_qty      =   $addon_qtys[$addon->id];
                            $addon_price    +=  ($addon_qty * $addon->price);
                            $addon_details[]  = array('id'      => $addon->id,
                                                    'name'      => $addon->name,
                                                    'price'     => $addon->price,
                                                    'quantity'  => $addon_qty);
                        }

                    }

                    $item_total_price = ($item_data->price + $addon_price) * $item_qty;

                    $order_detail_data[]  = array(
                        'order_id'        => $order->id,
                        'item_id'         => $item_id,
                        'quantity'        => $item_qty,
                        'addon_price'     => $addon_price,
                        'item_price'      => $item_price,
                        'total_price'     => $item_total_price,
                        'addon_details'   => json_encode($addon_details)
                    );

                    $total_amount += $item_total_price;
                }

                $actual_price = $total_amount;

                if (!empty($promo_code)) {
                    $discount_percent   = $is_valid_promo->discount_percent;

                    $discounted_price   = ($discount_percent * $total_amount) / 100;
                    $total_amount       -= $discounted_price;

                    $order->promo_code_id   = $is_valid_promo->id;
                    $order->promo_code      = $is_valid_promo->code;
                    $order->promo_code_discount_percent     = $discount_percent;
                    $order->discounted_price                = $discounted_price;
                }

                $order->actual_price    = $actual_price;
                $order->payment_type    = $payment_type;
                $order->total_amount    = $total_amount;
                $order->payment_status  = 1;
                $order->payment_details = $payment_details;
                $order->save();
                OrderDetail::insert($order_detail_data);
                DB::commit();
                return $this->sendResponse(["order_id"=> $order->id]);
            } catch(\Throwable $e) {
                DB::rollBack();
                Log::debug('Error found in store oreder',[ 'error' =>$e ]);
                return $this->sendError(__('messages.went-wrong'), 500);
            }
        }
        return $this->sendValidationError("message",trans("Provided promo code is invalid or expired"));
    }

    public function myOrders(Request $request)
    {
        $user                   = $request->user();
        $orders = Order::with('restaurant:id,name,image')->where('user_id',$user->id)->get(['id','user_id','restaurant_id','total_amount','created_at','order_processing','card_number','payment_method']);
        return $this->sendResponse($orders);
    }

    public function orderDetail(Request $request)
    {
        $request->validate(['id' => 'required|exists:orders,id,deleted_at,NULL']);
        $user   = $request->user();
        $id     = $request->id;
        $orders = Order::with(['restaurant:id,name,image',
                                'order_details:id,item_id,order_id,item_price,addon_price,total_price,addon_details,quantity,item_status',
                                'order_details.item:id,name,images',
                                'table:id,name'])
                        ->where('id',$id)
                        ->where('user_id',$user->id)
                        ->first(['id','user_id','restaurant_id','table_id','total_amount','discounted_price','actual_price','created_at','order_processing','card_number','payment_method']);
        return $this->sendResponse($orders);
    }


}
