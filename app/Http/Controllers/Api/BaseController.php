<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Classes\CustomEncrypt;
use App\Http\Controllers\Controller;

class BaseController extends Controller
{
	public function sendResponse($result = [])
	{
		$ecnrypter = new CustomEncrypt();
		return response()->json((request('env') != "test") ? $ecnrypter->encrypt($result) : $result, 200);
	}

	public function sendError($error, $code = 404, $errorMessages = [])
	{
		$response = [
			'message' => $error,
		];

		if(!empty($errorMessages)){
			$response['data'] = $errorMessages;
		}

		return response()->json($response, $code);
	}

	public function sendValidationError($field,$error, $code = 422, $errorMessages = [])
	{
		$response = [
			'errors' => [$field => $error],
			'message' => 'The given data was invalid.',
		];

		if(!empty($errorMessages)){
			$response['data'] = $errorMessages;
		}

		return response()->json($response, $code);
	}
}
