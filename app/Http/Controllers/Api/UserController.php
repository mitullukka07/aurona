<?php

namespace App\Http\Controllers\Api;


use App\Models\User;
use App\Mail\SendOtpMail;
use App\Models\CardDetail;
use Illuminate\Http\Request;
use App\Classes\CustomEncrypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\Api\LoginRequest;
use App\Http\Requests\Api\VerifyOtpRequest;
use App\Http\Requests\Api\UpdateUserProfile;
use App\Http\Requests\Api\RegistrationRequest;
use App\Http\Requests\Api\ResetPasswordRequest;
use App\Http\Requests\Api\ChangePasswordRequest;
use App\Http\Requests\Api\ForgotPasswordRequest;

class UserController extends BaseController
{
    public function registration(Request $request)
    {
        // dd($request->all());
        // social_type = '2-google, 1-Facbook, 3-Apple'
        $condition = [];
        $inputs    = $request->all();
        $id        = null;
        if (!empty($request->social_id) && !empty($request->social_type)) {
            switch ($request->social_type) {
                case 1:
                    $condition['google_id'] = $request->social_id;
                    $inputs['google_id'] = $request->social_id;
                    break;
                case 2:
                    $condition['facebook_id'] = $request->social_id;
                    $inputs['facebook_id'] = $request->social_id;
                    break;
                case 3:
                    $condition['apple_id']  = $request->social_id;
                    $inputs['apple_id']     = $request->social_id;
                    break;
                default:

                break;
            }
            $user = User::where($condition)->first();
            if ($user) {
                $id = $user->id;
            }else {
                if (isset($inputs['email']) && !empty($inputs['email'])) {
                    $user = User::where('email',$inputs['email'])->first();
                    if($user){
                        $id = $user->id;
                        $user->update($condition);
                    }
                }
            }
        }

        $rules = [
            'name'              => 'nullable|required_without:social_id',
            'password'          => 'nullable|required_without:social_id',
            'email'             => 'nullable|required_without:social_id|unique:users,email,'.$id,
            'phone'             => 'nullable|required_without:social_id|unique:users,phone,'.$id,
            'facebook_id'       => 'nullable|unique:users,facebook_id,'.$id,
            'google_id'         => 'nullable|unique:users,google_id,'.$id,
            'apple_id'          => 'nullable|unique:users,apple_id,'.$id,
            'device_token'      => 'required',
            'device_type'       => 'required',
            'social_id'         => 'nullable|required_without:password|required_with:social_type',
            'social_type'       => 'required_without:password|required_with:social_id',
        ];

        if ($request->social_type == 3) {
            $rules['email']         = 'nullable|required_without:social_id';
        }

        $request->validate($rules);

        DB::beginTransaction();

        try {
            unset($inputs['env']);
            unset($inputs['social_type']);

            if (empty($id)) {
                if (!empty($request->password)){
                    $otp                    = rand(1111,9999);
                    $inputs['password']     = Hash::make($request->password);
                    $inputs['otp']          = $otp;
                }
                $user = User::create($inputs);
            }else {
                $user->device_token = $request->device_token;
                $user->device_type  =  $request->device_type;
                $user->save();
            }

            $user = User::find($user->id);

            if ($user) {
                // Revoke Previous token Logout from all Device
                DB::table('oauth_access_tokens')
                ->where('user_id',$user->id)
                ->where('client_id',1)
                ->update(['revoked' => 1]);

                $user->token        = $user->createToken('MIM')->accessToken;
                DB::commit();

                return $this->sendResponse($user);
            }
            return $this->sendError(__('messages.went-wrong'), 500);
        } catch(\Throwable $e) {
            DB::rollBack();
            Log::debug('Registration : ',[ 'error' =>$e ]);
            return $this->sendError(__('messages.went-wrong'), 500);
        }
    }

    public function login(LoginRequest $request)
    {
        $user = User::where('phone',$request->phone)->first();
        if($user && Hash::check($request->password, $user->password)) {
            $user->device_type      =   $request->device_type;
            $user->device_token     =   $request->device_token;
            $user->current_version  =   $request->current_version;
            $user->save();

            $user = User::find($user->id);
            // Revoke Previous token Logout from all Device
            DB::table('oauth_access_tokens')
                ->where('user_id',$user->id)
                ->where('client_id',1)
                ->update(['revoked' => 1]);

            $user->token        = $user->createToken('MIM')->accessToken;
            return $this->sendResponse($user);
        } else {
            return $this->sendError(__('messages.auth_failed'), 401);
        }
    }

    public function forgotPassword(ForgotPasswordRequest $request)
    {
        $user = User::where('email', $request->email)->first();
        if ($user) {
            $token      = md5(uniqid(rand(), true));
            $user->otp  = rand(1111,9999);
            $user->remember_token = $token;
            $user->save();
            #Send OTP/Verification mail
            try {
                Mail::to($user->email)->send(new SendOtpMail($user));
            } catch(\Exception $e) {
                Log::debug('Send OTP mail : ',[ 'error' =>$e ]);
            }
            return $this->sendResponse(['verify_token' => $token]);
        }
        return $this->sendValidationError("email",trans("This email is not exist"));

    }

    public function verifyOtp(VerifyOtpRequest $request)
    {
        if ( $request->hasHeader('verify-token') ) {
            $verify_token = request()->header('verify-token');
            $user = User::where('remember_token', $verify_token)->first();
            if($user){
                if($user->otp == $request->otp){
                    $token                      = md5(uniqid(rand(), true));
                    $user->is_verify            = 1;
                    $user->email_verified_at    = date('Y-m-d H:i:s');
                    $user->otp                  = NULL;
                    $user->remember_token       = $token;
                    $user->save();

                    DB::table('oauth_access_tokens')
                    ->where('user_id',$user->id)
                    ->where('client_id',1)
                    ->update(['revoked' => 1]);

                    return $this->sendResponse(['verify_token' => $token]);
                }else {
                    return $this->sendValidationError("email",trans("Otp expired or invalid"));
                }
            } else {
                return $this->sendValidationError("message",trans("The provided verify token is incorrect"));
            }
        } else {
            return $this->sendValidationError("message",trans("The provided verify token is incorrect"));
        }
    }

    public function resetPassword(ResetPasswordRequest $request)
    {
        if ( request()->hasHeader('verify-token') ) {
            $verify_token = request()->header('verify-token');
            $user = User::where('remember_token', $verify_token)->first();
            if($user){
                $user->password         =   Hash::make($request->password);
                $user->remember_token   = NULL;
                $user->save();
                return $this->sendResponse(['message' => 'Password successfully reset']);
            } else {
                return $this->sendValidationError("message",trans("The provided verify token is incorrect"));
            }
        } else {
            return $this->sendValidationError("message",trans("The provided verify token is incorrect"));
        }
    }

    public function resendOtp(Request $request)
    {
        if ( request()->hasHeader('verify-token') ) {
            $verify_token = request()->header('verify-token');
            $user = User::where('remember_token', $verify_token)->first();
            if($user){
                $token                  = md5(uniqid(rand(), true));
                $user->otp              = rand(1111,9999);
                $user->is_verify        = 0;
                $user->remember_token   = $token;
                $user->save();
                try {
                    Mail::to($user->email)->send(new SendOtpMail($user));
                } catch(\Exception $e) {
                    Log::debug('Send OTP mail : ',[ 'error' =>$e ]);
                }
                return $this->sendResponse(['verify_token' => $token]);
            } else {
                return $this->sendValidationError("message",trans("The provided verify token is incorrect"));
            }
        } else {
            return $this->sendValidationError("message",trans("The provided verify token is incorrect"));
        }
    }



    public function changePassword(ChangePasswordRequest $request)
    {
        $user = $request->user();
        if($user) {
            if(Hash::check($request->old_password, $user->password)) {
                $user->password = Hash::make($request->new_password);
                $user->save();
                $user_data = getUserData($user->id);
                return $this->sendResponse($user_data);
            } else {
                return $this->sendValidationError('old_password', __('messages.password.old_wrong'));
            }
        }
    }



    public function logout(Request $request)
    {
        $user = $request->user();
        if($user) {
            $user->device_token     = NULL;
            $user->remember_token   = NULL;
            $user->save();
            DB::table('oauth_access_tokens')
                    ->where('user_id',$user->id)
                    ->where('client_id',1)
                    ->update(['revoked' => 1]);
            return $this->sendResponse(["message" => trans("Logout successfully")]);
        }
        return $this->sendError(__('messages.went-wrong'), 500);
    }

    public function getProfile(Request $request)
    {
    	$user_data = getUserData($request->user()->id);
        if($user_data)
            return $this->sendResponse($user_data);
        return $this->sendError(__('messages.went-wrong'), 500);
    }

    public function updateUserProfile(UpdateUserProfile $request)
    {
        $user = $request->user();
        if($user) {
            $user->name           =   $request->name ?: $user->name;
            $user->save();
	        $user_data = getUserData($user->id);
            return $this->sendResponse($user_data);
        } else {
            return $this->sendError(__('messages.user.user_not_found'), 404);
        }
    }

    public function storeCard(Request $request)
    {
        $user   = $request->user();
        $id     = $user->id;
        $request->validate(['card_number'       => 'required|numeric|unique:card_details,card_number,NULL,id,deleted_at,NULL,user_id,'.$id,
                            'card_holder_name'  => 'required',
                            'expire'            =>  'required']);
        if($user) {
            $inputs = $request->all();
            $inputs['user_id'] = $id;
            $card_details = CardDetail::create($inputs);
            return $this->sendResponse($card_details);
        } else {
            return $this->sendError(__('messages.user.user_not_found'), 404);
        }
    }

    public function updateCard(Request $request)
    {
        $user       = $request->user();
        $user_id    = $user->id;
        $request->validate(['card_id'           => 'required|exists:card_details,id',
                            'card_number'       => 'required|numeric|unique:card_details,card_number,'.$request->card_id.',id,user_id,'.$user_id.',deleted_at,NULL',
                            'card_holder_name'  => 'required',
                            'expire'            =>  'required']);
        if($user) {
            $inputs             = $request->all();
            $inputs['user_id']  = $user_id;
            $card_details       = CardDetail::find($request->card_id);
            $card_details->update($inputs);
            return $this->sendResponse($card_details);
        } else {
            return $this->sendError(__('messages.user.user_not_found'), 404);
        }
    }

    public function deleteCard(Request $request)
    {
        $user   = $request->user();
        $id     = $user->id;
        $request->validate(['card_id'           => 'required|exists:card_details,id',]);
        if($user) {
            CardDetail::where('id',$request->card_id)->where('user_id',$id)->delete();
            return $this->sendResponse(["message" => trans("Card removed successfully")]);
        } else {
            return $this->sendError(__('messages.user.user_not_found'), 404);
        }
    }


    public function cardList(Request $request)
    {
        $user   = $request->user();
        $id     = $user->id;
        if($user) {
            $cards = CardDetail::where('user_id',$id)->get();
            return $this->sendResponse($cards);
        } else {
            return $this->sendError(__('messages.user.user_not_found'), 404);
        }
    }
}
