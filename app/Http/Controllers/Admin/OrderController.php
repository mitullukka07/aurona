<?php

namespace App\Http\Controllers\Admin;

use App\Models\Order;
use Illuminate\Http\Request;
use App\Contracts\OrderContract;
use App\DataTables\OrderDataTable;
use App\Http\Controllers\Controller;
use App\Models\OrderDetail;

class OrderController extends Controller
{
    protected $model;

    public function __construct(OrderContract $contract)
    {
        $this->model = $contract;
    }

    public function index(OrderDataTable $datatable)
    {
        return $datatable->render('admin.order.index');
    }


    public function show($id)
    {
        if($id) {
            $data =  $this->model->with(['restaurant','user','table','user_details']);
            $data =  $this->model->show($id);
            return view('admin.order.show', compact('data'))->render();
        }
    }

    public function orderCompleted(Request $request)
    {
        $data_id  = $request->data_id;
        $res['msg']    = 'Something went wrong';
        $res['action'] = 'Cancelled!';
        $res['status'] = 'error';
        if($data_id) {
            $data =  $this->model->with(['order_details']);
            $data =  $this->model->findData($data_id);

            if($data) {
                $data->order_processing = 2;
                $data->order_details()->update(['item_status'=>2]);
                $data->save();
                $res['msg']     = 'Order status completed successfully.';
                $res['action']  = 'Completed!';

                $res['status']  = 'success';
                $res['datatable_id'] = 'order-table';
            }
        }
        return $res;
    }

    public function orderReadyToTake(Request $request)
    {
        $data_id  = $request->data_id;
        $res['msg']    = 'Something went wrong';
        $res['action'] = 'Cancelled!';
        $res['status'] = 'error';
        if($data_id) {
            $data =  OrderDetail::find($data_id);
            if($data) {
                $data->item_status = 2;
                $data->save();
                $res['msg']     = 'Item ready to take.';
                $res['action']  = 'Completed!';

                $res['status']  = 'success';
            }
        }
        return $res;
    }
}
