<?php

namespace App\Http\Controllers\Admin;

use App\Models\Addon;
use Illuminate\Http\Request;
use App\Contracts\AddonContract;
use App\DataTables\AddonDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Addon\StoreRequest;

class AddonController extends Controller
{
    protected $model;

    public function __construct(AddonContract $contract)
    {
        $this->model = $contract;
    }

    public function index(AddonDataTable $datatable)
    {
        return $datatable->render('admin.addon.index');
    }

    public function create()
    {
        $data =  new Addon();
        return view('admin.addon.add_edit', compact('data'))->render();
    }


    public function store(StoreRequest $request)
    {
        $inputs = $request->all();

        if(isset($inputs['is_active']) && $inputs['is_active']) {
            $inputs['is_active'] = 1;
        }

        if (!empty($request->id)) {
            $data = $this->model->update($inputs, $request->id);
        }else{
            $data = $this->model->create($inputs);
        }

        if($data) {
            $response['status']     = 'success';
            $response['message']    = 'Addon added successfully';
            $response['datatable_id']   = 'addon-table';
        } else {
            $response['status']     = 'danger';
            $response['message']    = 'Something went wrong! Try again later...';
        }
        return $response;
    }


    public function show($id)
    {
        if($id) {
            $data =  $this->model->show($id);
            return view('admin.addon.show', compact('data'))->render();
        }
    }


    public function edit($id)
    {
        if($id) {
            $data =  $this->model->edit($id);
            return view('admin.addon.add_edit', compact('data'))->render();
        }
    }


    public function destroy($id)
    {
        $res = $this->model->delete($id);

        if($res) {
            $data['msg']    = 'Addon Deleted successfully.';
            $data['action'] = 'Deleted!';
            $data['status'] = 'success';
            $data['datatable_id'] = 'addon-table';
        } else {
            $data['msg']    = 'Something went wrong';
            $data['action'] = 'Cancelled!';
            $data['status'] = 'error';
        }
        return $data;
    }

    public function changeStatus(Request $request)
    {
        $res = $this->model->changeStatus($request->all());

        if($res) {
            if($res['status'] == 0)
            {
                $data['msg'] = 'Addon Inactivated successfully.';
                $data['action'] = 'Inactivated!';
            } else {
                $data['msg'] = 'Addon Activated successfully.';
                $data['action'] = 'Activated!';
            }
            $data['status'] = 'success';
            $data['datatable_id'] = 'addon-table';
        } else {

            $data['msg'] = 'Something went wrong';
            $data['action'] = 'Cancelled!';
            $data['status'] = 'error';
        }

        return $data;
    }
}
