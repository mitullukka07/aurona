<?php

namespace App\Http\Controllers\Admin;

use App\Models\PromoCode;
use Illuminate\Http\Request;
use App\Contracts\PromoCodeContract;
use App\Http\Controllers\Controller;
use App\DataTables\PromoCodeDataTable;
use App\DataTables\PromoCodeUserDataTable;
use App\Http\Requests\Admin\PromoCode\StoreRequest;

class PromoCodeController extends Controller
{
    protected $model;

    public function __construct(PromoCodeContract $contract)
    {
        $this->model = $contract;
    }

    public function index(PromoCodeDataTable $datatable)
    {
        return $datatable->render('admin.promo_code.index');
    }

    public function create()
    {
        $data =  new PromoCode();
        return view('admin.promo_code.add_edit', compact('data'))->render();
    }


    public function store(StoreRequest $request)
    {
        $inputs = $request->all();
        if(isset($inputs['image']) && $inputs['image']) {
            $inputs['image'] = $inputs['image']->store('PromoCode','public');
        }
        if(isset($inputs['is_banner']) && $inputs['is_banner']) {
            $inputs['is_banner'] = 1;
        }
        if (!empty($request->id)) {
            $data = $this->model->update($inputs, $request->id);
        }else{
            $data = $this->model->create($inputs);
        }

        if($data) {
            $response['status']     = 'success';
            $response['message']    = 'Promo Code added successfully';
            $response['datatable_id']   = 'promocode-table';
        } else {
            $response['status']     = 'danger';
            $response['message']    = 'Something went wrong! Try again later...';
        }
        return $response;
    }


    public function show($id)
    {
        if($id) {
            $data =  $this->model->show($id);
            return view('admin.promo_code.show', compact('data'))->render();
        }
    }


    public function edit($id)
    {
        if($id) {
            $data =  $this->model->edit($id);
            return view('admin.promo_code.add_edit', compact('data'))->render();
        }
    }


    public function destroy($id)
    {
        $res = $this->model->delete($id);

        if($res) {
            $data['msg']    = 'Promo Code Deleted successfully.';
            $data['action'] = 'Deleted!';
            $data['status'] = 'success';
            $data['datatable_id'] = 'promocode-table';
        } else {
            $data['msg']    = 'Something went wrong';
            $data['action'] = 'Cancelled!';
            $data['status'] = 'error';
        }
        return $data;
    }

    public function changeStatus(Request $request)
    {
        $res = $this->model->changeStatus($request->all());

        if($res) {
            if($res['status'] == 0)
            {
                $data['msg'] = 'Promo Code Inactivated successfully.';
                $data['action'] = 'Inactivated!';
            } else {
                $data['msg'] = 'Promo Code Activated successfully.';
                $data['action'] = 'Activated!';
            }
            $data['status'] = 'success';
            $data['datatable_id'] = 'promocode-table';
        } else {

            $data['msg'] = 'Something went wrong';
            $data['action'] = 'Cancelled!';
            $data['status'] = 'error';
        }

        return $data;
    }


    // promo code user

    public function promoCodeUser(PromoCodeUserDataTable $datatable)
    {
        return $datatable->render('admin.promo_code_user.index');
    }

    public function destroyUserPromoCode(Request $request)
    {
        $promocode_id   = $request->promocode_id;
        $user_id        = $request->user_id;
        $res            = $this->model->show($promocode_id);

        if($res && $user_id) {
            $res->users()->detach($user_id);
            $data['msg']    = 'Promo Code Deleted successfully.';
            $data['action'] = 'Deleted!';
            $data['status'] = 'success';
            $data['datatable_id'] = 'promocodeuser-table';
        } else {
            $data['msg']    = 'Something went wrong';
            $data['action'] = 'Cancelled!';
            $data['status'] = 'error';
        }
        return $data;
    }

}
