<?php

namespace App\Http\Controllers\Admin;

use App\Models\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contracts\SettingContract;
use App\Http\Requests\Admin\Setting\StoreAdminSettingRequest;

class SettingController extends Controller
{
    protected $model;

    public function __construct(SettingContract $contract)
    {
        $this->model = $contract;
    }

    public function index()
    {
        $admin_setting = Setting::where('key','admin_setting')->first();
        return view('admin.setting.index',compact('admin_setting'));
    }

    public function create()
    {
        $data =  new Setting();
        return view('admin.setting.admin_setting', compact('data'))->render();
    }


    public function store(StoreAdminSettingRequest $request)
    {
        $inputs = $request->all();
        unset($inputs['_token']);
        if (isset($inputs['id']) && !empty($inputs['id'])) {
            if($request->has('image')) {
                $inputs['image'] = $inputs['image']->store('Setting','public');
            }else{
                $inputs['image'] = $inputs['old_image'];
            }
            unset($inputs['old_image']);
            $data   = $this->model->update($inputs, $inputs['id']);
        }else{
            if($request->has('image')) {
                $inputs['image'] = $inputs['image']->store('Setting','public');
            }
            $input  = array('key'=> 'admin_setting', 'value' => $inputs);
            $data   = $this->model->create($input);
        }

        if($data) {
            $response['status']     = 'success';
            $response['message']    = 'Setting added successfully';
            $response['datatable_id']   = 'Settingdatatable-table';
        } else {
            $response['status']     = 'danger';
            $response['message']    = 'Something went wrong! Try again later...';
        }
        return $response;
    }


    public function show($id)
    {
        if($id) {
            $data =  $this->model->show($id);
            return view('admin.setting.show', compact('data'))->render();
        }
    }


    public function edit($id)
    {
        if($id) {
            $res =  $this->model->edit($id);
            if ($res) {
                $data       = $res->value;
                $data['id'] = $id;
            }
            return view('admin.setting.admin_setting', compact('data'))->render();
        }
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        $res = $this->model->delete($id);

        if($res) {
            $data['msg']    = 'Setting Deleted successfully.';
            $data['action'] = 'Deleted!';
            $data['status'] = 'success';
            $data['datatable_id'] = 'Settingdatatable-table';
        } else {
            $data['msg']    = 'Something went wrong';
            $data['action'] = 'Cancelled!';
            $data['status'] = 'error';
        }
        return $data;
    }
}
