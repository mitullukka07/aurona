<?php

namespace App\Http\Controllers\Admin;

use App\Contracts\TableContract;
use App\DataTables\TableDataTable;
use App\Models\Table;
use App\Models\Restaurant;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Table\StoreRequest;

class TableController extends Controller
{
    protected $model;

    public function __construct(TableContract $contract)
    {
        $this->model = $contract;
    }

    public function index(TableDataTable $datatable)
    {
        return $datatable->render('admin.table.index');
    }

    public function create()
    {
        $data       = new Table();
        $restaurants   = Restaurant::where('is_active',1)->pluck('name','id');
        return view('admin.table.add_edit', compact('data','restaurants'))->render();
    }


    public function store(StoreRequest $request)
    {
        $inputs = $request->all();
        if(isset($inputs['is_active']) && $inputs['is_active']) {
            $inputs['is_active'] = 1;
        }
        if (!empty($request->id)) {
            $data = $this->model->update($inputs, $request->id);
        }else{
            $data       = $this->model->create($inputs);
            $qr_code    = storeQrCodeImages($data->restaurant_id,$data->id);
            $data->qr_code = $qr_code;
            $data->save();
        }

        if($data) {
            $response['status']     = 'success';
            $response['message']    = 'Table added successfully';
            $response['datatable_id']   = 'table-table';
        } else {
            $response['status']     = 'danger';
            $response['message']    = 'Something went wrong! Try again later...';
        }
        return $response;
    }


    public function show($id)
    {
        if($id) {
            $data =  $this->model->show($id);
            return view('admin.table.show', compact('data'))->render();
        }
    }


    public function edit($id)
    {
        if($id) {
            $data =  $this->model->edit($id);
            $restaurants   = Restaurant::where('is_active',1)->pluck('name','id');
            return view('admin.table.add_edit', compact('data','restaurants'))->render();
        }
    }


    public function destroy($id)
    {
        $res = $this->model->delete($id);

        if($res) {
            $data['msg']    = 'Table Deleted successfully.';
            $data['action'] = 'Deleted!';
            $data['status'] = 'success';
            $data['datatable_id'] = 'table-table';
        } else {
            $data['msg']    = 'Something went wrong';
            $data['action'] = 'Cancelled!';
            $data['status'] = 'error';
        }
        return $data;
    }

    public function changeStatus(Request $request)
    {
        $res = $this->model->changeStatus($request->all());

        if($res) {
            if($res['status'] == 0)
            {
                $data['msg'] = 'Table Inactivated successfully.';
                $data['action'] = 'Inactivated!';
            } else {
                $data['msg'] = 'Table Activated successfully.';
                $data['action'] = 'Activated!';
            }
            $data['status'] = 'success';
            $data['datatable_id'] = 'table-table';
        } else {

            $data['msg'] = 'Something went wrong';
            $data['action'] = 'Cancelled!';
            $data['status'] = 'error';
        }

        return $data;
    }
}
