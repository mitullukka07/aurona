<?php

namespace App\Http\Controllers\Admin;

use App\Models\Cms;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contracts\CmsContract;
use App\DataTables\CmsDataTable;
use App\Http\Requests\Admin\Cms\StoreRequest;

class CmsController extends Controller
{
    protected $model;

    public function __construct(CmsContract $contract)
    {
        $this->model = $contract;
    }

    public function index(CmsDataTable $datatable)
    {
        return $datatable->render('admin.cms.index');
    }

    public function create()
    {
        $data =  new Cms();
        return view('admin.cms.add_edit', compact('data'))->render();
    }


    public function store(StoreRequest $request)
    {
        $inputs = $request->all();
        if (!empty($request->id)) {
            $data = $this->model->update($inputs, $request->id);
        }else{
            $data = $this->model->create($inputs);
        }

        if($data) {
            $response['status']     = 'success';
            $response['message']    = 'Restaurant added successfully';
            $response['datatable_id']   = 'cms-table';
        } else {
            $response['status']     = 'danger';
            $response['message']    = 'Something went wrong! Try again later...';
        }
        return $response;
    }


    public function show($id)
    {
        if($id) {
            $data =  $this->model->show($id);
            return view('admin.cms.show', compact('data'))->render();
        }
    }


    public function edit($id)
    {
        if($id) {
            $data =  $this->model->edit($id);
            return view('admin.cms.add_edit', compact('data'))->render();
        }
    }


    public function destroy($id)
    {
        $res = $this->model->delete($id);

        if($res) {
            $data['msg']    = 'Cms Deleted successfully.';
            $data['action'] = 'Deleted!';
            $data['status'] = 'success';
            $data['datatable_id'] = 'cms-table';
        } else {
            $data['msg']    = 'Something went wrong';
            $data['action'] = 'Cancelled!';
            $data['status'] = 'error';
        }
        return $data;
    }

    public function changeStatus(Request $request)
    {
        $res = $this->model->changeStatus($request->all());

        if($res) {
            if($res['status'] == 0) {
                $data['msg']    = 'Cms Inactivated successfully.';
                $data['action'] = 'Inactivated!';
            } else {
                $data['msg']    = 'Cms Activated successfully.';
                $data['action'] = 'Activated!';
            }
            $data['status']         = 'success';
            $data['datatable_id']   = 'cms-table';
        } else {
            $data['msg']    = 'Something went wrong';
            $data['action'] = 'Cancelled!';
            $data['status'] = 'error';
        }
        return $data;
    }
}
