<?php

namespace App\Http\Controllers\Admin;

use App\Models\Restaurant;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contracts\RestaurantContract;
use App\DataTables\RestaurantDataTable;
use App\Http\Requests\Admin\Restaurant\StoreRequest;
use App\Models\Category;

class RestaurantController extends Controller
{
    protected $model;

    public function __construct(RestaurantContract $contract)
    {
        $this->model = $contract;
    }

    public function index(RestaurantDataTable $datatable)
    {
        return $datatable->render('admin.restaurant.index');
    }

    public function create()
    {
        $data =  new Restaurant();
        $category   = Category::where('is_active',1)->pluck('name','id');
        return view('admin.restaurant.add_edit', compact('data','category'))->render();
    }


    public function store(StoreRequest $request)
    {
        $inputs = $request->all();
        if(isset($inputs['image']) && $inputs['image']) {
            $inputs['image'] = $inputs['image']->store('Restaurant','public');
        }
        if (!empty($request->id)) {
            $data = $this->model->update($inputs, $request->id);
        }else{
            $data = $this->model->create($inputs);
        }

        if($data) {
            $response['status']     = 'success';
            $response['message']    = 'Restaurant added successfully';
            $response['datatable_id']   = 'restaurantdatatable-table';
        } else {
            $response['status']     = 'danger';
            $response['message']    = 'Something went wrong! Try again later...';
        }
        return $response;
    }


    public function show($id)
    {
        if($id) {
            $data =  $this->model->show($id);
            return view('admin.restaurant.show', compact('data'))->render();
        }
    }


    public function edit($id)
    {
        if($id) {
            $data       =  $this->model->edit($id);
            $category   = Category::where('is_active',1)->pluck('name','id');
            return view('admin.restaurant.add_edit', compact('data','category'))->render();
        }
    }


    public function destroy($id)
    {
        $res = $this->model->delete($id);

        if($res) {
            $data['msg']    = 'Restaurant Deleted successfully.';
            $data['action'] = 'Deleted!';
            $data['status'] = 'success';
            $data['datatable_id'] = 'restaurantdatatable-table';
        } else {
            $data['msg']    = 'Something went wrong';
            $data['action'] = 'Cancelled!';
            $data['status'] = 'error';
        }
        return $data;
    }

    public function changeStatus(Request $request)
    {
        $res = $this->model->changeStatus($request->all());

        if($res) {
            if($res['status'] == 0)
            {
                $data['msg'] = 'Restaurant Inactivated successfully.';
                $data['action'] = 'Inactivated!';
            } else {
                $data['msg'] = 'Restaurant Activated successfully.';
                $data['action'] = 'Activated!';
            }
            $data['status'] = 'success';
            $data['datatable_id'] = 'restaurantdatatable-table';
        } else {

            $data['msg'] = 'Something went wrong';
            $data['action'] = 'Cancelled!';
            $data['status'] = 'error';
        }

        return $data;
    }
}
