<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\Admin;
use Illuminate\Http\Request;
use App\Contracts\UserContract;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Contracts\RestaurantContract;

class DashboardController extends Controller
{
    protected $restaurantContract;
    protected $userContract;

	public function __construct(RestaurantContract $restaurantContract, UserContract $userContract)
	{
		$this->middleware('auth');
        $this->userContract         = $userContract;
        $this->restaurantContract   = $restaurantContract;
	}


	public function index()
	{
        $customers          = $this->userContract->get();
        $restaurants        = $this->restaurantContract->get();
        $total_customer     = $customers->count();
        $total_restaurant   = $restaurants->count();
        $recent_customer    = $customers->sortByDesc('created_at')->take(10);
		return view('admin.dashboard',compact('customers','total_customer','recent_customer','restaurants','total_restaurant'));
	}

	public function changePassword()
	{
		return view('admin.auth.change-pass');
	}

	public function updatePassword(Request $request)
	{
		$request->validate([
			                   'old_pass' => 'required',
			                   'new_pass' => 'required|confirmed|min:6'
		                   ]);
		$id         = Auth::id();
		$selectData = Admin::where('id', $id)->first();
		$password   = $selectData->password;

		if(Hash::check($request->old_pass, $password))
		{
			$newPass        = bcrypt($request->new_pass);
			$updatePassword = Admin::where('id', $id)->update(['password' => $newPass]);
			if($updatePassword)
			{
				Auth::guard('admin')->logout();
				return redirect()->route('admin.login');
			}
			return redirect()->back()->withSuccess('Password Update Successfully...');
		} else {
			return redirect()->back()->withDanger('Old Password does not match with our database');
		}
	}
}
