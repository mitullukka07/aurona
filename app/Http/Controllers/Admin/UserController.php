<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Illuminate\Http\Request;
use App\Contracts\BaseContract;
use App\DataTables\UserDataTable;
use App\Http\Controllers\Controller;
use App\Contracts\UserContract;
use App\Http\Requests\Admin\Customer\StoreRequest;

class UserController extends Controller
{
    protected $model;

    public function __construct(UserContract $userContract)
    {
        $this->model = $userContract;
    }

    public function index(UserDataTable $datatable)
    {
        return $datatable->render('admin.customer.index');
    }


    public function create()
    {
        $data =  new User();
        return view('admin.customer.add_edit', compact('data'))->render();
    }


    public function store(StoreRequest $request)
    {
        if (!empty($request->id)) {
            $data = $this->model->update($request->all(), $request->id);
        }else{
            $data = $this->model->create($request->all());
        }

        if($data) {
            $response['status']         = 'success';
            $response['message']        = 'Customer added successfully';
            $response['datatable_id']   = 'userdatatable-table';
        } else {
            $response['status']         = 'danger';
            $response['message']        = 'Something went wrong! Try again later...';
        }
        return $response;
    }


    public function show($id)
    {
        if($id) {
            $data =  $this->model->show($id);
            return view('admin.customer.show', compact('data'))->render();
        }
    }


    public function edit($id)
    {
        if($id) {
            $data =  $this->model->edit($id);
            return view('admin.customer.add_edit', compact('data'))->render();
        }
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        $res = $this->model->delete($id);

        if($res) {
            $data['msg'] = 'Customer Deleted successfully.';
            $data['action'] = 'Deleted!';
            $data['status'] = 'success';
            $data['datatable_id'] = 'userdatatable-table';
        } else {
            $data['msg'] = 'Something went wrong';
            $data['action'] = 'Cancelled!';
            $data['status'] = 'error';
        }
        return $data;
    }

    public function changeStatus(Request $request)
    {
        $res = $this->model->changeStatus($request->all());

        if($res) {
            if($res['status'] == 0)
            {
                $data['msg'] = 'Customer Inactivated successfully.';
                $data['action'] = 'Inactivated!';
            } else {
                $data['msg'] = 'Customer Activated successfully.';
                $data['action'] = 'Activated!';
            }
            $data['status'] = 'success';
            $data['datatable_id'] = 'userdatatable-table';
        } else {

            $data['msg'] = 'Something went wrong';
            $data['action'] = 'Cancelled!';
            $data['status'] = 'error';
        }

        return $data;
    }
}
