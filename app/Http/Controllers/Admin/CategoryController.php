<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Contracts\CategoryContract;
use App\Http\Controllers\Controller;
use App\DataTables\CategoryDataTable;
use App\Http\Requests\Admin\Category\StoreRequest;
use App\Models\Addon;

class CategoryController extends Controller
{
    protected $model;

    public function __construct(CategoryContract $contract)
    {
        $this->model = $contract;
    }

    public function index(CategoryDataTable $datatable)
    {
        return $datatable->render('admin.category.index');
    }

    public function create()
    {
        $data =  new Category();
        $addons = Addon::where('is_active',1)->pluck('name','id');
        return view('admin.category.add_edit', compact('data','addons'))->render();
    }

    public function store(StoreRequest $request)
    {
        $inputs = $request->all();
        if(isset($inputs['image']) && $inputs['image']) {
            $inputs['image'] = $inputs['image']->store('Category','public');
        }
        if (!empty($request->id)) {
            $data = $this->model->update($inputs, $request->id);
        }else{
            $data = $this->model->create($inputs);
        }

        if($data) {
            $response['status']     = 'success';
            $response['message']    = 'Category added successfully';
            $response['datatable_id']   = 'category-table';
        } else {
            $response['status']     = 'danger';
            $response['message']    = 'Something went wrong! Try again later...';
        }
        return $response;

    }


    public function edit($id)
    {
        if($id) {
            $data   =  $this->model->edit($id);
            $addons = Addon::where('is_active',1)->pluck('name','id');
            return view('admin.category.add_edit', compact('data','addons'))->render();
        }
    }

    public function destroy($id)
    {
        $res = $this->model->delete($id);

        if($res) {
            $data['msg']    = 'Category Deleted successfully.';
            $data['action'] = 'Deleted!';
            $data['status'] = 'success';
            $data['datatable_id'] = 'category-table';
        } else {
            $data['msg']    = 'Something went wrong';
            $data['action'] = 'Cancelled!';
            $data['status'] = 'error';
        }
        return $data;
    }

    public function changeStatus(Request $request)
    {
        $res = $this->model->changeStatus($request->all());

        if($res) {
            if($res['status'] == 0) {
                $data['msg']    = 'Category Inactivated successfully.';
                $data['action'] = 'Inactivated!';
            } else {
                $data['msg']    = 'Category Activated successfully.';
                $data['action'] = 'Activated!';
            }
            $data['status'] = 'success';
            $data['datatable_id'] = 'category-table';
        } else {
            $data['msg']    = 'Something went wrong';
            $data['action'] = 'Cancelled!';
            $data['status'] = 'error';
        }
        return $data;
    }
}
