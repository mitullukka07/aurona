<?php

namespace App\Http\Controllers\Admin;

use App\Models\Notification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contracts\NotificationContract;
use App\DataTables\NotificationDataTable;
use App\Http\Requests\Admin\Notification\StoreRequest;

class NotificationController extends Controller
{
    protected $model;

    public function __construct(NotificationContract $contract)
    {
        $this->model = $contract;
    }

    public function index(NotificationDataTable $datatable)
    {
        return $datatable->render('admin.notification.index');
    }

    public function destroy($id)
    {
        $res = $this->model->delete($id);

        if($res) {
            $data['msg']    = 'Notification Deleted Successfully.';
            $data['action'] = 'Deleted!';
            $data['status'] = 'success';
            $data['datatable_id'] = 'notification-table';
        } else {
            $data['msg']    = 'Something went wrong';
            $data['action'] = 'Cancelled!';
            $data['status'] = 'error';
        }
        return $data;
    }


}
