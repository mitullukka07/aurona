<?php

namespace App\Http\Controllers\Admin;

use App\Contracts\ItemContract;
use App\Models\Item;
use Illuminate\Http\Request;
use App\DataTables\ItemDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Item\StoreRequest;
use App\Models\Category;
use App\Models\Restaurant;

class ItemController extends Controller
{
    protected $model;

    public function __construct(ItemContract $contract)
    {
        $this->model = $contract;
    }

    public function index(ItemDataTable $datatable)
    {
        return $datatable->render('admin.item.index');
    }

    public function create()
    {
        $data =  new Item();
        $restaurants = Restaurant::where('is_active',1)->pluck('name','id');
        $category    = Category::where('is_active',1)->pluck('name','id');
        return view('admin.item.add_edit', compact('data','restaurants','category'))->render();
    }


    public function store(StoreRequest $request)
    {
        $inputs = $request->all();

        if(isset($inputs['images']) && $inputs['images']) {
            $inputs['images'] = $inputs['images']->store('Item','public');
        }
        if(isset($inputs['is_active']) && $inputs['is_active']) {
            $inputs['is_active'] = 1;
        }
        if (!empty($request->id)) {
            $data = $this->model->update($inputs, $request->id);
        }else{
            $data = $this->model->create($inputs);
        }

        if($data) {
            $response['status']     = 'success';
            $response['message']    = 'Item added successfully';
            $response['datatable_id']   = 'item-table';
        } else {
            $response['status']     = 'danger';
            $response['message']    = 'Something went wrong! Try again later...';
        }
        return $response;
    }


    public function show($id)
    {
        if($id) {
            $data =  $this->model->with(['restaurant','category']);
            $data =  $this->model->show($id);
            return view('admin.item.show', compact('data'))->render();
        }
    }


    public function edit($id)
    {
        if($id) {
            $data =  $this->model->edit($id);
            $restaurants = Restaurant::where('is_active',1)->pluck('name','id');
            $restaurant  = Restaurant::find($data->restaurant_id);
            $category    = getRestaurantCategory($restaurant->category_ids);
            if (!empty($category)) {
                $category  = $category->pluck('name','id');
            }
            return view('admin.item.add_edit', compact('data','restaurants','category'))->render();
        }
    }

    public function destroy($id)
    {
        $res = $this->model->delete($id);

        if($res) {
            $data['msg']    = 'Restaurant Deleted successfully.';
            $data['action'] = 'Deleted!';
            $data['status'] = 'success';
            $data['datatable_id'] = 'item-table';
        } else {
            $data['msg']    = 'Something went wrong';
            $data['action'] = 'Cancelled!';
            $data['status'] = 'error';
        }
        return $data;
    }

    public function changeStatus(Request $request)
    {
        $res = $this->model->changeStatus($request->all());

        if($res) {
            if($res['status'] == 0)
            {
                $data['msg'] = 'Item Inactivated successfully.';
                $data['action'] = 'Inactivated!';
            } else {
                $data['msg'] = 'Item Activated successfully.';
                $data['action'] = 'Activated!';
            }
            $data['status'] = 'success';
            $data['datatable_id'] = 'item-table';
        } else {

            $data['msg'] = 'Something went wrong';
            $data['action'] = 'Cancelled!';
            $data['status'] = 'error';
        }

        return $data;
    }

    public function categoryList(Request $request)
    {
        if($request->restaurant_id) {
            $restaurant = Restaurant::find($request->restaurant_id);
            $category   = getRestaurantCategory($restaurant->category_ids)->pluck('name','id');
            return view('admin.item.category', compact('category'))->render();
        }
    }
}
