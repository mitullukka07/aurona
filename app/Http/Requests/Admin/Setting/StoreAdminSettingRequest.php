<?php

namespace App\Http\Requests\Admin\Setting;

use Illuminate\Foundation\Http\FormRequest;

class StoreAdminSettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =  [
            'name'          =>  'required|max:50',
            'email'         =>  'required|max:100',
            'mobile'        =>  'required|numeric|digits:10',
            'address'       =>  'required',
            'description'   =>  'required',
            'image'         =>  'mimes:jpg,png,jpeg',
        ];

        return $rules;
    }
}
