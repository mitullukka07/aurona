<?php

namespace App\Http\Requests\Admin\Item;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = request()->id;

        $rules =  [
            'name'          =>  'required|unique:items,name,'.$id,
            'category_id'   =>  'required',
            'restaurant_id' =>  'required',
            'images'        =>  'required|mimes:jpg,png,jpeg',
        ];

        if (!empty($id)) {
            $rules['images'] = 'mimes:jpg,png,jpeg';
        }

        return $rules;
    }
}
