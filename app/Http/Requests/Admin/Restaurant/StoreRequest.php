<?php

namespace App\Http\Requests\Admin\Restaurant;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = request()->id;

        $rules =  [
            'name'      =>  'required|max:50',
            'email'     =>  'required|max:100|unique:restaurants,email,'.$id,
            'mobile'    =>  'required|numeric|digits:10',
            'address'   =>  'required',
            'lat'       =>  'required|numeric',
            'long'      =>  'required|numeric',
            'category_ids'      =>  'required',
            'image'             =>  'required|mimes:jpg,png,jpeg',
        ];

        if (!empty($id)) {
            $rules['image'] = 'mimes:jpg,png,jpeg';
        }

        return $rules;
    }
}
