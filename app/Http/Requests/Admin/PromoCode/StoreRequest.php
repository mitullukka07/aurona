<?php

namespace App\Http\Requests\Admin\PromoCode;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = request()->id;

        $rules =  [
            'title'             =>  'required|max:100|unique:promo_codes,title,'.$id,
            'code'              =>  'required|min:6|max:6|unique:promo_codes,code,'.$id,
            'discount_percent'  =>  'required|integer|between:1,99',
            'start_date'        =>  'required|date',
            'end_date'          =>  'date|after:start_date',
        ];

        return $rules;
    }
}
