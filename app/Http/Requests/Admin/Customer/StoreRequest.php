<?php

namespace App\Http\Requests\Admin\Customer;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = request()->id;

        $rules =  [
            'name'      =>  'required|max:50',
            'email'     =>  'required|max:100|unique:users,email,'.$id,
            'phone'     =>  'required|numeric|digits:10|unique:users,phone,'.$id,
        ];

        return $rules;
    }
}
