<?php
namespace App\Contracts;

interface RestaurantContract
{
    public function all();
    public function get();
    public function create(array $data);
    public function delete(int $id);
    public function update(array $data, $id);
    public function changeStatus(array $data);
}
