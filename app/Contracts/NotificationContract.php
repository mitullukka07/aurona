<?php
namespace App\Contracts;

interface NotificationContract
{
    public function all();
    public function get();
    public function create(array $data);
    public function delete(int $id);
    public function update(array $data, $id);
    public function changeStatus(array $data);
}
