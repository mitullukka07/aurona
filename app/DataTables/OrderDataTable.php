<?php

namespace App\DataTables;

use App\Models\Order;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class OrderDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', function($data){
                $result = "";
                $result .= '<a href="'.route('admin.order.show', $data->id).'" class="btn btn-primary btn-sm js_display_in_modal" title="View"><i class="fa fa-eye"></i></a>';

                return $result;
            })

            ->addColumn('order_status', function($data){
                $result =   '<label class="switch" title="Click complete to order status">
                            <input type="checkbox" name="is_active" class="form-control changeOrderStatus" data-url="'.route('admin.order.order_complete').'" data-id="'.$data->id.'" >
                            <span class="slider"></span>
                            </label>';
                if ($data->order_processing == 2) {
                    $result ='<span class="badge badge-success">Completed</span>';
                }

                return $result;
            })
            ->addIndexColumn()
            ->rawColumns(['action', 'order_status']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Order $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Order $model)
    {
        return $model->newQuery()->with(['user','restaurant','table']);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('order-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Blfrtip')
                    ->orderBy([1,'DESC'])
                    ->buttons(
                        Button::make('create'),
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('no')->data('DT_RowIndex')->searchable(false)->orderable(false),
            Column::make('id')->hidden(true),
            Column::make('user.name')->title('Customer'),
            Column::make('restaurant.name')->title('Restaurant'),
            Column::make('table.name')->title('Table'),
            Column::make('total_amount'),
            Column::make('order_status'),
            Column::computed('action')
                  ->exportable(false)
                  ->printable(false)
                  ->width(60)
                  ->addClass('text-center text-nowrap'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Order_' . date('YmdHis');
    }
}
