<?php

namespace App\DataTables;

use App\Models\PromoCodeUser;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class PromoCodeUserDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', function($data){
                $result = "";
                $result .= '<button type="button" url="'.route('admin.promo-codes.users_delete').'" class="btn btn-sm btn-danger round js_delete_custom" title="Delete" user_id="'.$data->user->id.'" promocode_id="'.$data->promo_code->id.'"><i class="fa fa-trash"></i></button>';

                return $result;
            })
            ->addIndexColumn();
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\PromoCodeUser $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(PromoCodeUser $model)
    {
        return $model->newQuery()->with(['user','promo_code']);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('promocodeuser-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Blfrtip')
                    ->buttons(
                        Button::make('create'),
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('no')->data('DT_RowIndex')->searchable(false)->orderable(false),
            Column::make('user.name')->title('Customer'),
            Column::make('promo_code.title')->title('Title'),
            Column::make('promo_code.code')->title('Code'),
            Column::make('promo_code.start_date')->title('Start Date'),
            Column::make('promo_code.end_date')->title('End Date'),
            Column::computed('action')
                  ->exportable(false)
                  ->printable(false)
                  ->width(60)
                  ->addClass('text-center text-nowrap'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'PromoCodeUser_' . date('YmdHis');
    }
}
