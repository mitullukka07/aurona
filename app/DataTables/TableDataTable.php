<?php

namespace App\DataTables;

use App\Models\Table;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class TableDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', function($data){
                $result = "";
                if($data->is_active == 1) {
                    $result .= ' <button type="button" url="'.route('admin.table.change_status').'" class="btn btn-success btn-sm changeStatus" status="0" title="click to Inactivate" data_id="'.$data->id.'"><i class="fa fa-unlock"></i></button> ';
                } else {
                    $result .= ' <button type="button" url="'.route('admin.table.change_status').'" class="btn btn-secondary btn-sm changeStatus" status="1" title="click to Activate" data_id="'.$data->id.'"><i class="fa fa-lock">&nbsp;</i></button> ';
                }
                $result .= '<a href="'.route('admin.table.show', $data->id).'" class="btn btn-primary btn-sm js_display_in_modal" title="View"><i class="fa fa-eye"></i></a>
                <a href="'.route('admin.table.edit', $data->id).'" class="btn btn-info btn-sm js_display_in_modal" title="Edit"><i class="fa fa-edit"></i></a>';

                return $result;
            })
            ->addIndexColumn();
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Table $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Table $model)
    {
        return $model->newQuery()->with('restaurant');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('table-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Blfrtip')
                    ->orderBy([1,'DESC'])
                    ->buttons(
                        Button::make('create'),
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('no')->data('DT_RowIndex')->searchable(false)->orderable(false),
            Column::make('id')->hidden(true),
            Column::make('name'),
            Column::make('restaurant.name')->title('Restaurant'),
            Column::computed('action')
                  ->exportable(false)
                  ->printable(false)
                  ->width(60)
                  ->addClass('text-center text-nowrap'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Table_' . date('YmdHis');
    }
}
