<?php

namespace App\DataTables;

use App\Models\User;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class UserDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', function($data){
                $result = "";
                if($data->is_active == 1) {
                    $result .= ' <button type="button" url="'.route('admin.customers.change_status').'" class="btn btn-success btn-sm changeStatus" status="0" title="click to Inactivate" data_id="'.$data->id.'"><i class="fa fa-unlock"></i></button> ';
                } else {
                    $result .= ' <button type="button" url="'.route('admin.customers.change_status').'" class="btn btn-secondary btn-sm changeStatus" status="1" title="click to Activate" data_id="'.$data->id.'"><i class="fa fa-lock">&nbsp;</i></button> ';
                }
                $result .= '<a href="'.route('admin.customers.show', $data->id).'" class="btn btn-primary btn-sm js_display_in_modal" title="View View"><i class="fa fa-eye"></i></a>
                <button type="button" url="'.route('admin.customers.destroy',$data->id).'" class="btn btn-sm btn-danger round js_delete_record" data_id="'.$data->id.'"><i class="fa fa-trash"></i></button>';

                // <a href="'.route('admin.customers.edit', $data->id).'" class="btn btn-success btn-sm js_display_in_modal" title="Edit"><i class="fa fa-edit"></i></a>

                return $result;
            })
            ->addIndexColumn();
    }

    /**
     * Get query source of dataTable.
     *
     * @param \User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(User $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('userdatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Blfrtip')
                    ->orderBy([1,'DESC'])
                    ->buttons(
                        Button::make('create'),
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('no')->data('DT_RowIndex')->searchable(false)->orderable(false),
            Column::make('id')->hidden(true),
            Column::make('name'),
            Column::make('email'),
            Column::make('phone'),
            Column::computed('action')
                  ->exportable(false)
                  ->printable(false)
                  ->width(60)
                  ->addClass('text-center text-nowrap'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'User_' . date('YmdHis');
    }
}
