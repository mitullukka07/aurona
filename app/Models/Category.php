<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use HasFactory;
    // , SoftDeletes

    protected $guarded  = ['env'];

    protected $casts    = ['addon_ids' => 'array'];

    public function getImageAttribute($value)
    {
        return $value ? asset('storage/'.$value) : NULL;
    }

    public function items() {
        return $this->hasMany(Item::class);
    }
}
