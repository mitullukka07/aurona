<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PromoCode extends Model
{
    use HasFactory;

    protected $guarded = ['env'];

    public function getImageAttribute($value)
    {
        return $value ? asset('storage/'.$value) : NULL;
    }

    public function users() {
        return $this->belongsToMany(User::class);
    }
}
