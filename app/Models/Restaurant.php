<?php

namespace App\Models;

use App\Models\Category;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Restaurant extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = ['env'];

    protected $casts   = ['category_ids' => 'array'];

    protected $appends = ['rating'];

    public function getImageAttribute($value)
    {
        return $value ? asset('storage'.'/'.$value) : NULL;
    }

    public function ratings() {
        return $this->hasMany(Rating::class);
    }


    public function getRatingAttribute(){
        $rating_sum     = $this->ratings()->sum('rating');
        $rating_count   = $this->ratings()->count();
        $rating_percent = 0;
        if ($rating_sum > 0 && $rating_count > 0) {
            $rating_percent = ($rating_sum * 100) / ($rating_count * 2);
        }
        return $rating_percent;
    }
}
