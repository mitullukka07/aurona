<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Table extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = ['env'];

    public function restaurant() {
        return $this->belongsTo(Restaurant::class);
    }

    public function getQrCodeAttribute($value)
    {
        return $value ? asset('storage/qr_code/'.$value) : NULL;
    }
}
