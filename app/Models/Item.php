<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use HasFactory;

    protected $guarded  = ['env'];

    // It will be single image thats why commenting
    // protected $casts    = ['images' => 'array'];

    public function getImagesAttribute($value)
    {
        return $value ? asset('storage'.'/'.$value) : NULL;
    }

    public function restaurant() {
        return $this->belongsTo(Restaurant::class);
    }

    public function category() {
        return $this->belongsTo(Category::class);
    }
}
