<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderDetail extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = ['env'];

    protected $casts    = ['addon_details' => 'array'];

    public function item() {
        return $this->belongsTo(Item::class);
    }
}
