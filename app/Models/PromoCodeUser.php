<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PromoCodeUser extends Model
{
    use HasFactory;


    protected $table = 'promo_code_user';

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function promo_code() {
        return $this->belongsTo(PromoCode::class);
    }
}
