<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    protected $guarded = ['env'];

    public function user() {
        return $this->belongsTo(User::class);
    }
}
