    function scroll_to_error(){
        $('html, body').animate({
            scrollTop: $('.has-error:first').offset()}, 1300);
    }

    function scroll_top(){
        $('html, body').animate({
            scrollTop: $('body').offset().top-80
        }, 1100);
    }

    function fade_effect(time = null){
       $('#js_main_row').fadeToggle(time);
    }

    $("body").on('click',".js_display_in_modal",function(e){
        e.preventDefault();
        var url = $(this).attr('href');
        $.ajax({
            type: "get",
            url: url,
            success: function (response) {
                $('#js_show_modal').find('.modal-content').html(response);
                $('#js_show_modal').modal("show");
            }
         });
    })


    function redirect_url(url=null){
        // fade_effect();
        $.get(url, {}, function(data){
            $('#js_main_row').html(data);
            fade_effect('slow');
            scroll_top();
        });
    }

    $("body").on('click',"#js_submit_ajax",function(){
        $('.js_error_span').html('');
        my_form = $("#js_main_row").find("#js_form");
        my_form.find('.has-error').removeClass("has-error");
        my_form.find('.invalid').removeClass("invalid");

        var form_data = new FormData(my_form[0]);
        var form_url = $("#js_form").attr('action');
        $.ajax({
            type:"Post",
            url:form_url,
            data:form_data,
            contentType: false,
            cache: false,
            processData:false,
            success:function(result){

                if (result.status) {
                    if (result.redirect_url != '') {
                        redirect_url(result.redirect_url);
                    }
                }else{;
                    $.each(result.errors, function( k, v ){
                        my_form.find('#'+k).addClass('invalid');
                        my_form.find('#'+k).parent('div .form-group').find('.js_error_span').html(v);
                    });
                    scroll_to_error();
                }
            }
        })
    })

    $('body').on('submit', '#js_modal_form', function(e){
        e.preventDefault();
        $('body').find('.has-danger').removeClass('has-danger');
        $(this).find('.error-msg-input').html("");
        if(jquery_validation($(this).find('.js_valid'))){
            my_modal    = $('body').find("#js_show_modal");
            url         = $(this).attr('action');
            form        = $(this);
            submit_form_ajax(form, my_modal, url);
        }
    });

    function submit_form_ajax(form, my_modal, url) {
        $.ajax({
            url: url,
            type: 'post',
            processData: false,
            contentType: false,
            // data : new FormData(form),
            data : new FormData(form[0]),
            success: function(result){
                if(result) {
                    $html = '<div class="alert alert-block alert-'+result.status+'"><button type="button" class="close" data-dismiss="alert">×</button><strong>'+result.message+'</strong></div>';

                    $('.ajax-msg').append($html);
                }
                my_modal.modal('hide');
                window.LaravelDataTables[result.datatable_id].draw();
            },
            complete:function(){
                window.setTimeout(function() {
                    $(".alert").fadeTo(500, 0).slideUp(500, function(){
                        $(this).remove();
                    });
                }, 3000);
            },
            error: function (data) {
                if(data.status === 422) {
                    var errors = $.parseJSON(data.responseText);
                    $.each(errors.errors, function (key, value) {
                        my_modal.find('#'+key).parents('.form-group').addClass('has-danger');
                        my_modal.find('#'+key).parents('.form-group')
                        .find('.error-msg-input').html(value);
                    });
                }
            }
        });
    }

    function jquery_validation(my_each) {
        flag = true;
        my_each.each(function() {
            t = $(this);
            val = $.trim($(this).val());
            if (t.hasClass('required')) {
                if (val == "" || val == null) {
                    error = 'This field is required!';
                    $(this).parents('.form-group').addClass('has-danger');;
                    $(this).parents('.form-group').find('.error-msg-input').html(error);
                    flag = false;
                }
            }

            if (val != "") {
                // numeric value will check
                if (t.hasClass('number') && isNaN(val)) {
                    error = 'Please enter only numeric value!';
                    $(this).parents('.form-group').find('.error-msg-input').html(error);
                    flag = false;
                }

                // Email validation
                const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

                if (t.hasClass('email') && !re.test(String(val).toLowerCase())) {
                    error = 'Please enter valid email!';
                    $(this).parents('.form-group').find('.error-msg-input').html(error);
                    flag = false;
                }
            }
        })
        return flag;
    }

    // change status
    $(document).on('click', '.changeStatus', function() {
        var this_var = this;
        $(this).attr('disabled', true);
        var data_id = $(this).attr('data_id');
        var status  = $(this).attr('status');
        var url     = $(this).attr('url');
        var msg = "";

        if(status == 0) {
            msg = "Inactive";
        } else {
            msg = "Active";
        }

        swal({
            title: 'Are you sure want to '+msg+'?',
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, '+msg+' it!',
            reverseButtons: true
            }).then((result) => {
                if (result) {
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url : url,
                        type : 'PATCH',
                        data : {'status' : status, 'data_id' : data_id},
                        success : function (res) {

                            swal(
                                res.action, //get from controller (block/unblock/cancel)
                                res.msg, // get from controller
                                res.status // get from controller (success/error)
                            )
                            window.LaravelDataTables[res.datatable_id].draw();
                        }
                    });
                } else {
                    swal("Cancelled", "Status not changed :)", "error");
                    $(this).attr('disabled', false);
                }
        }).catch((error) => {
            $('.changeStatus').attr('disabled', false);
        });

    });

    //delete
    $(document).on('click', '.js_delete_record', function() {
        var url = $(this).attr('url');
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!',
            reverseButtons: true
        }).then((result) => {
            if (result) {
                $.ajax({
                    type:'DELETE',
                    url: url,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success:function(res) {
                        if (res) {
                            swal(
                                res.action, //get from controller (block/unblock/cancel)
                                res.msg, // get from controller
                                res.status // get from controller (success/error)
                            )
                            window.LaravelDataTables[res.datatable_id].draw();
                        }

                    }
                });
            } else {
                swal("Cancelled", "Status not changed :)", "error");
            }
        }).catch((error) => {
            $('.js_delete_record').attr('disabled', false);
        });
    });


